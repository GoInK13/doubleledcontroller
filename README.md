# DoubleLEDController  
Control 4×RGB strip with 2 D1mini  

## HARD:  
### PCB
One wimos connected to wifi, IR 2×RGB, MIC.  
One arduino nano connected to wemos, 2×RGB  
![](Doc/PCB_Top_KiCAD.png)


### Connection :  
#### Wemos :  
```
BUILTIN_LED : D4
LED0_R : D1
LED0_G : D2
LED0_B : D3
LED1_R : D5
LED1_G : D6
LED1_B : D7
Neopix : D8
MIC    : A0
TX : nano RX
RX : nano TX
```
#### Nano :  
```
LED2_R : D3
LED2_G : D5
LED2_B : D6
LED3_R : D9
LED3_G : D10
LED3_B : D11
TX : wemos RX
RX : wemos TX
```

### Interface  
![](Doc/Server_192.168.1.240.png)

### Prog
For wemos : Use type of card : LOLIN(wemos) D1 R2 & mini


Remote ?
https://www.battech.de/tech-reviews/mini-wifi-esp-wroom-02-modul/
