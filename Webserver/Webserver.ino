/*********
	Rui Santos
	Complete project details at https://RandomNerdTutorials.com/esp8266-nodemcu-web-server-slider-pwm/

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files.

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	D0R255\n
*********/

// Import required libraries
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "credentials.h"
#include <IRremote.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>


#define DBG_UART	1	//Set to 1 if connect terminal

#define Do_LED0_R	D1
#define Do_LED0_G	D2
#define Do_LED0_B	D3
#define Do_DUMMY	D4	//LED_BUILTIN
#define Do_LED1_R	D5
#define Do_LED1_G	D6
#define Do_LED1_B	D7
#define IR_RECV_PIN D0

#define IR_VP		0xA35CFF00
#define IR_VM		0xA25DFF00
#define IR_NEXT		0xBE41FF00
#define IR_POWER	0xBF40FF00
#define IR_RED		0xA758FF00
#define IR_ORANGE1	0xAB54FF00
#define IR_ORANGE2	0xAF50FF00
#define IR_ORANGE3	0xE31CFF00
#define IR_YELLOW	0xE718FF00
#define IR_GREEN	0xA659FF00
#define IR_GREEN1	0xAA55FF00
#define IR_CYAN1	0xAE51FF00
#define IR_CYAN2	0xE21DFF00
#define IR_CYAN3	0xE619FF00
#define IR_BLUE		0xBA45FF00
#define IR_BLUE1	0xB649FF00
#define IR_PURPLE1	0xB24DFF00
#define IR_PURPLE2	0xE11EFF00
#define IR_PINK		0xE51AFF00
#define IR_WHITE	0xBB44FF00
#define IR_LIGHT1	0xB34CFF00
#define IR_LIGHT2	0xB34CFF00
#define IR_LIGHT3	0xE01FFF00
#define IR_LIGHT4	0xE41BFF00
#define IR_JUMP3	0xEB14FF00
#define IR_JUMP7	0xEA15FF00
#define IR_FADE3	0xE916FF00
#define IR_FADE7	0xE817FF00
#define IR_QUICK	0xEF10FF00
#define IR_SLOW		0xEE11FF00
#define IR_FLASH	0xED12FF00
#define IR_AUTO		0xEC13FF00
#define IR_T1H		0xF30CFF00
#define IR_T2H		0xF20DFF00
#define IR_T4H		0xF10EFF00
#define IR_T8H		0xF00FFF00
#define IR_MUSIC1	0xF708FF00
#define IR_MUSIC2	0xF609FF00
#define IR_MUSIC3	0xF50AFF00
#define IR_MUSIC4	0xF40BFF00

#define COLOR_R		0
#define COLOR_G		1
#define COLOR_B		2

#define POS_NORTH	0
#define POS_MIDDLE	1
#define POS_SOUTH	2
#define POS_EXT		3

//Defines for animations
#define ANIM_FADE3	1
#define ANIM_FADE7	2
#define ANIM_JUMP3	3
#define ANIM_JUMP7	4

#define ANIM_STOP	5
#define ANIM_WHITE	6

#define ANIM_FIRE	7
#define ANIM_JUNGLE	8
#define ANIM_WATER	9
#define ANIM_PINKY	10

#define ANIM_AUTOFADE		11
#define ANIM_DISCO			12
#define ANIM_EPILEPTIQUE	13
#define ANIM_TV				14

String sliderValue = "0";

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);
AsyncWebSocket ws("/ws");
IPAddress ip(192, 168, 1, 241); // where xx is the desired IP Address
IPAddress gateway(192, 168, 1, 254); // set gateway to match your network

IRrecv irrecv2(IR_RECV_PIN);
decode_results results2;
//Better transition
const int correctPWM[256]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,19,21,23,25,27,30,32,35,37,40,43,46,49,52,55,59,62,66,69,73,77,81,85,89,94,98,103,107,112,117,122,127,133,138,144,149,155,161,167,173,179,185,192,198,205,212,219,226,233,240,248,255,263,271,279,287,295,303,312,320,329,338,347,356,365,374,383,393,403,413,422,433,443,453,464,474,485,496,507,518,529,540,552,564,575,587,599,611,624,636,649,661,674,687,700,714,727,741,754,768,782,796,810,825,839,854,868,883,898,913,929,944,960,975,991,1007,1023,1040,1056,1073,1089,1106,1123,1140,1158,1175,1193,1210,1228,1246,1264,1282,1301,1319,1338,1357,1376,1395,1414,1434,1453,1473,1493,1513,1533,1553,1573,1594,1615,1636,1657,1678,1699,1720,1742,1764,1786,1808,1830,1852,1875,1897,1920,1943,1966,1989,2013,2036,2060,2084,2108,2132,2156,2180,2205,2230,2254,2279,2305,2330,2355,2381,2407,2433,2459,2485,2511,2538,2564,2591,2618,2645,2673,2700,2728,2755,2783,2811,2839,2868,2896,2925,2954,2983,3012,3041,3071,3100,3130,3160,3190,3220,3250,3281,3312,3342,3373,3404,3436,3467,3499,3531,3562,3595,3627,3659,3692,3724,3757,3790,3823,3857,3890,3924,3958,3992,4026,4060,4095};
const int correctDelay[256]={-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,19,21,23,25,27,30,32,35,37,40,43,46,49,52,55,59,62,66,69,73,77,81,85,89,94,98,103,107,112,117,122,127,133,138,144,149,155,161,167,173,179,185,192,198,205,212,219,226,233,240,248,255,263,271,279,287,295,303,312,320,329,338,347,356,365,374,383,393,403,413,422,433,443,453,464,474,485,496,507,518,529,540,552,564,575,587,599,611,624,636,649,661,674,687,700,714,727,741,754,768,782,796,810,825,839,854,868,883,898,913,929,944,960,975,991,1007,1023,1040,1056,1073,1089,1106,1123,1140,1158,1175,1193,1210,1228,1246,1264,1282,1301,1319,1338,1357,1376,1395,1414,1434,1453,1473,1493,1513,1533,1553,1573,1594,1615,1636,1657,1678,1699,1720,1742,1764,1786,1808,1830,1852,1875,1897,1920,1943,1966,1989,2013,2036,2060,2084,2108,2132,2156,2180,2205,2230,2254,2279,2305,2330,2355,2381,2407,2433,2459,2485,2511,2538,2564,2591,2618,2645,2673,2700,2728,2755,2783,2811,2839,2868,2896,2925,2954,2983,3012,3041,3071,3100,3130,3160,3190,3220,3250,3281,3312,3342,3373,3404,3436};

const int DELAY_REDUCE_BRIGHTNESS = 20;	//Delay in ms, so max is 20*255ms

void SetColor(byte position, byte colorR, byte colorG, byte colorB);
void SetFullColor(byte l_position, unsigned int l_value);
void SetDelay(byte position, byte l_delay);
void SetArrayUint(byte position, unsigned int array[4], byte l_value);
void SetArrayByte(byte position, byte array[4], byte l_value);
int SetRandColor(byte currentColor, byte limitMin, byte limitMax, int randMin, byte randMax);

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Plafond Salon</title>
	<style>
		html {font-family: Arial; display: inline-block; text-align: center;}
		h2 {font-size: 2em;margin-top:0; margin-bottom:0.2em;}
		p {font-size: 1.9rem;}
		button {font-size: 1.0rem;padding: 1rem 0.5rem; width:100%;}
		caption {font-size: 1.2em;}
		tablebutton {border-spacing: 1em 0;}
		table {text-align: center;}
		tableSlider {padding-bottom:25px}
		body {max-width:30rem; margin:0px auto; padding-bottom: 1em;}
		.sliderBig { -webkit-appearance: none; height: 2rem; width: 100%; background:linear-gradient(to right, #FF0000, #ffff00, #00ff00, #00ffff, #0000ff, #ff00ff, #ff00ff); }
		.sliderBig::-webkit-slider-thumb {-webkit-appearance: none; width: 1rem; height: 2.5rem; background : #A0A0A0}
		.sliderRed { -webkit-appearance: none; height: 2rem; width: 100%; background:linear-gradient(to right, #000000, #FF0000); }
		.sliderRed::-webkit-slider-thumb {-webkit-appearance: none; width: 1rem; height: 2.5rem; background : #A0A0A0}
		.sliderGreen { -webkit-appearance: none; height: 2rem; width: 100%; background:linear-gradient(to right, #000000, #00FF00); }
		.sliderGreen::-webkit-slider-thumb {-webkit-appearance: none; width: 1rem; height: 2.5rem; background : #A0A0A0}
		.sliderBlue { -webkit-appearance: none; height: 2rem; width: 100%; background:linear-gradient(to right, #000000, #0000FF); }
		.sliderBlue::-webkit-slider-thumb {-webkit-appearance: none; width: 1rem; height: 2.5rem; background : #A0A0A0}
		.sliderBrightness { -webkit-appearance: none; height: 2rem; width: 100%; background:linear-gradient(to right, #000000, #FFFFFF); }
		.sliderBrightness::-webkit-slider-thumb {-webkit-appearance: none; width: 1rem; height: 2.5rem; background : #FFFF00}
		.sliderDelay { height: 2rem; width: 100%;)}
	</style>
</head>
<body>
	<h2>Plafond du salon</h2>
	<table style="width:100%">
	<tr>
		<td><input type="checkbox" id="cbNor" checked=""><label for="cbNor">Nord</label></td>
		<td><input type="checkbox" id="cbMid" checked=""><label for="cbMid">Milieu</label></td>
		<td><input type="checkbox" id="cbSou" checked=""><label for="cbSou">Sud</label></td>
		<td><input type="checkbox" id="cbExt" checked=""><label for="cbExt">Exter</label></td>
	</tr>
	</table>
	<table class="tableSlider" style="width:100%">
	<caption>Couleur / luminosite</caption>
	<tbody>
	<tr>
	<td colspan="3" style="padding-bottom:0.5rem">
		<input class="sliderBrightness" oninput="updateSliderPWM(this, 'Brightness')" type="range" id="pwmSlider_Brightness" min="0" max="255" step="1" value="100">
	</td>
	</tr>
	<tr>
		<td  colspan="3" style="padding-bottom:0.5rem">
			<input class="sliderBig" oninput="updateSliderPWM(this, 'Color')" type="range" id="pwmSlider_Color" min="0" max="1530" step="1" value="0">
		</td>
	</tr>
	<tr>
		<td>
			<input class="sliderRed" oninput="updateSliderPWMUnic('Red')" type="range" id="pwmSlider_Red" min="0" max="255" step="1" value="0">
		</td>
		<td>
			<input class="sliderGreen" oninput="updateSliderPWMUnic('Green')" type="range" id="pwmSlider_Green" min="0" max="255" step="1" value="0">
		</td>
		<td>
			<input class="sliderBlue" oninput="updateSliderPWMUnic('Blue')" type="range" id="pwmSlider_Blue" min="0" max="255" step="1" value="0">
		</td>
	</tr>
	</tbody></table>
	<hr size="1">
	<table class="tablebutton" style="width:100%">
	<tbody>
	<tr>
		<td colspan="2">Mode : <cssmode id="idMode">Aucun</cssmode></td>
		<td colspan="2">Blanc : <cssmode id="idBlanc">100</cssmode>ms</td>
	</tr>
	<tr>
		<td style="width:25%"><button onclick="clickButton('fade3')">Fade3</button></td>
		<td style="width:25%"><button onclick="clickButton('fade7')">Fade7</button></td>
		<td style="width:25%"><button onclick="clickButton('jump3')">Jump3</button></td>
		<td style="width:25%"><button onclick="clickButton('jump7')">Jump7</button></td>
	</tr>
	<tr>
		<td colspan="2" style="width:50%">Vitesse:<br>
			<input class="sliderDelay" oninput="updateSliderDelay()" type="range" id="pwmSlider_Delay" min="15" max="70" step="1" value="30"></td>
		<td style="width:25%"><button onclick="clickButton('stop')">Stop</button></td>
		<td style="width:25%"><button onclick="clickButton('white')">Blanc</button></td>
	</tr>
	</tbody></table>
	<hr size="1">
	<table class="tablebutton" style="width:100%">
	<caption>Animation</caption>
	<tbody>
	<tr>
		<td style="width:25%"><button onclick="clickButton('fire')">Fire</button></td>
		<td style="width:25%"><button onclick="clickButton('jungle')">Jungle</button></td>
		<td style="width:25%"><button onclick="clickButton('water')">Water</button></td>
		<td style="width:25%"><button onclick="clickButton('pinky')"><3</button></td>
	</tr>
	<tr>
		<td style="width:25%"><button onclick="clickButton('autofade')">AutoFade</button></td>
		<td style="width:25%"><button onclick="clickButton('disco')">Disco</button></td>
		<td style="width:25%"><button onclick="clickButton('epileptique')">Epileptique</button></td>
		<td style="width:25%"><button onclick="clickButton('tv')">TV</button></td>
	</tr>
	</tbody></table>
	<hr size="1">
	<table style="width:100%">
	<caption>Timeout</caption>
		<tr>
			<td>
				Il est <cssTime id="currentTime">17:23</cssTime> <br>
				<label for="appt">Heure d'extinction :</label>
				<input type="time" id="timeout" name="timeout"> <br>
				Stop dans <cssDelayOff id="delayOff">00:00</cssDelayOff> <cssTimeoutValid id="timeoutValid"></cssTimeoutValid>
			</td>
			<td>
				<button onclick="validTimeout()">Valider</button1>
			</td>
		</tr>
	<table>
	<table style="width:100%">
		<td>
			<button onclick="AddDelay(-30)">-30min</button1>
		</td>
		<td>
			<button onclick="AddDelay(-5)">-5min</button1>
		</td>
		<td>
			<button onclick="AddDelay(5)">+5min</button1>
		</td>
		<td>
			<button onclick="AddDelay(30)">+30min</button1>
		</td>
	</table>
<script>


function updateSliderPWM(element, type) {
	var position = document.getElementById("cbNor").checked*1 + document.getElementById("cbMid").checked*2 + document.getElementById("cbSou").checked*4 + document.getElementById("cbExt").checked*8;
	var xhr = new XMLHttpRequest();
	if (type=='Brightness') {
		var sliderBrightness = document.getElementById("pwmSlider_Brightness").value;
		xhr.open("GET", "/slider?position="+position+"&brightness="+sliderBrightness, true);
		console.log("Brightness="+sliderBrightness);
	} else {
		var sliderColor = document.getElementById("pwmSlider_Color").value;
		xhr.open("GET", "/slider?position="+position+"&color="+sliderColor, true);
		console.log("Color="+sliderColor);
	}
	xhr.send();
	var jsSlider = {"type":type, "Color":document.getElementById("pwmSlider_Color").value, "Brightness":document.getElementById("pwmSlider_Brightness").value};
	websocket.send(JSON.stringify(jsSlider));
}

function updateSliderPWMUnic(color){
	var position = document.getElementById("cbNor").checked*1 + document.getElementById("cbMid").checked*2 + document.getElementById("cbSou").checked*4 + document.getElementById("cbExt").checked*8;
	var sliderR = document.getElementById("pwmSlider_Red").value;
	var sliderG = document.getElementById("pwmSlider_Green").value;
	var sliderB = document.getElementById("pwmSlider_Blue").value;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/slider?position="+position+"&r="+sliderR+"&g="+sliderG+"&b="+sliderB, true);
	xhr.send();
	var jsColor = {"color":color, "Red":sliderR, "Green":sliderG, "Blue":sliderB};
	websocket.send(JSON.stringify(jsColor));
}

function updateSliderDelay(){
	var delay = parseInt(document.getElementById("pwmSlider_Delay").value);
	var position = document.getElementById("cbNor").checked*1 + document.getElementById("cbMid").checked*2 + document.getElementById("cbSou").checked*4 + document.getElementById("cbExt").checked*8;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/button?delay="+delay+"&position="+position, true);
	xhr.send();
	var jsDelay = {"delay":delay};
	websocket.send(JSON.stringify(jsDelay));
}

function validTimeout(){
	var objRef = {delayH: 0, delayM: 0, delayS:0, delayAllS:0};
	GetDelayBeforeOff(objRef);
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/delayOff?seconds="+objRef.delayAllS, true);
	xhr.send();
	var jsTimeout = {"timeout":document.getElementById("timeout").value}
	websocket.send(JSON.stringify(jsTimeout));
}

function buttonToAnimationName(buttonName){
	var animationName;
	if (buttonName=='fade3' || buttonName==1) {	//Fade3
		animationName = "Fade3";
	} else if (buttonName=='fade7' || buttonName==2) {	//Fade7
		animationName = "Fade7";
	} else if (buttonName=='jump3' || buttonName==3) {	//Jump3
		animationName = "Jump3";
	} else if (buttonName=='jump7' || buttonName==4) {	//Jump7
		animationName = "Jump7";
	} else if (buttonName=='stop' || buttonName==5) {	//Stop
		animationName = "aucun";
	} else if (buttonName=='white' || buttonName==6) {	//Full white
		animationName = "White";
	} else if (buttonName=='fire' || buttonName==7) {	//Fire
		animationName = "Fire";
	} else if (buttonName=='jungle' || buttonName==8) {	//Jungle
		animationName = "Jungle";
	} else if (buttonName=='water' || buttonName==9) {	//Water
		animationName = "Water";
	} else if (buttonName=='pinky' || buttonName==10) {	//Pinky
		animationName = "Pinky";
	} else if (buttonName=='autofade' || buttonName==11) {	//AutoFade
		animationName = "AutoFade";
	} else if (buttonName=='disco' || buttonName==12) {	//Disco
		animationName = "Disco";
	} else if (buttonName=='epileptique' || buttonName==13) {	//Epileptique
		animationName = "Epileptique";
	} else if (buttonName=='tv' || buttonName==14) {	//TV
		animationName = "TV";
	} else {
		animationName="Stop";
	}
	return animationName;
}

function clickButton(buttonName){
	console.log(buttonName);
	var xhr = new XMLHttpRequest();
	var position = document.getElementById("cbNor").checked*1 + document.getElementById("cbMid").checked*2 + document.getElementById("cbSou").checked*4 + document.getElementById("cbExt").checked*8;
	var buttonNumber=5;	//Stop
	if (buttonName=='fade3' || buttonName==1) {	//Fade3
		buttonNumber=1;
	} else if (buttonName=='fade7' || buttonName==2) {	//Fade7
		buttonNumber=2;
	} else if (buttonName=='jump3' || buttonName==3) {	//Jump3
		buttonNumber=3;
	} else if (buttonName=='jump7' || buttonName==4) {	//Jump7
		buttonNumber=4;
	} else if (buttonName=='stop' || buttonName==5) {	//Stop
		buttonNumber=5;
	} else if (buttonName=='white' || buttonName==6) {	//Full white
		buttonNumber=6;
		document.getElementById("pwmSlider_Red").value=255;
		document.getElementById("pwmSlider_Green").value=255;
		document.getElementById("pwmSlider_Blue").value=255;
	} else if (buttonName=='fire' || buttonName==7) {	//Fire
		buttonNumber=7;
	} else if (buttonName=='jungle' || buttonName==8) {	//Jungle
		buttonNumber=8;
	} else if (buttonName=='water' || buttonName==9) {	//Water
		buttonNumber=9;
	} else if (buttonName=='pinky' || buttonName==10) {	//Pinky
		buttonNumber=10;
	} else if (buttonName=='autofade' || buttonName==11) {	//AutoFade
		buttonNumber=11;
	} else if (buttonName=='disco' || buttonName==12) {	//Disco
		buttonNumber=12;
	} else if (buttonName=='epileptique' || buttonName==13) {	//Epileptique
		buttonNumber=13;
	} else if (buttonName=='tv' || buttonName==14) {	//TV
		buttonNumber=14;
	}
	document.getElementById("idMode").innerHTML = buttonToAnimationName(buttonName);
	xhr.open("GET", "/button?value="+buttonNumber+"&position="+position, true);
	xhr.send();
	var jsIdMode = {"idMode":document.getElementById("idMode").innerHTML}
	websocket.send(JSON.stringify(jsIdMode));
}

function GetDelayBeforeOff(pDelay){
	var today = new Date();
	var futureOff = new Date();
	futureOff.setHours(parseInt(document.getElementById("timeout").value[0])*10+parseInt(document.getElementById("timeout").value[1]));
	futureOff.setMinutes(parseInt(document.getElementById("timeout").value[3])*10+parseInt(document.getElementById("timeout").value[4]));
	futureOff.setSeconds(0);
	if (futureOff < today) {
		futureOff.setDate(futureOff.getDate() + 1);
	}
	pDelay.delayAllS = (futureOff - today)/1000;
	pDelay.delayH = parseInt(pDelay.delayAllS/3600);
	pDelay.delayM = parseInt((pDelay.delayAllS - pDelay.delayH*3600)/60);
	pDelay.delayS = (pDelay.delayAllS - pDelay.delayH*3600 - pDelay.delayM*60)%60;
}

function AddDelay(newDiff){
	var jsTimeout = document.getElementById("timeout").value;
	console.log("Click on button : "+newDiff+", "+jsTimeout);
	if(jsTimeout==''){
		var today = new Date();
		today.setMinutes(today.getMinutes()+parseInt(newDiff));
		console.log(today);
		var time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0');
		document.getElementById("timeout").value = time;
	} else {
		var today = new Date();
		today.setHours(parseInt(document.getElementById("timeout").value[0])*10+parseInt(document.getElementById("timeout").value[1]));
		today.setMinutes(parseInt(document.getElementById("timeout").value[3])*10+parseInt(document.getElementById("timeout").value[4])+parseInt(newDiff));
		today.setSeconds(0);
		var time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0');
		document.getElementById("timeout").value = time;
	}
	UpdateTime();
}

//Update time every seconds
function UpdateTime(){
	var today = new Date();
	var time = String(today.getHours()).padStart(2, '0') + ":" + String(today.getMinutes()).padStart(2, '0') + ":" + String(today.getSeconds()).padStart(2, '0');
	document.getElementById("currentTime").innerHTML = time;
	var objRef = {delayH: 0, delayM: 0, delayS:0, delayAllS:0};
	GetDelayBeforeOff(objRef);
	document.getElementById("delayOff").innerHTML = String(objRef.delayH).padStart(2, '0') + ":" + String(objRef.delayM).padStart(2, '0') + ":" + String(objRef.delayS).padStart(2, '0');
}
var t=setInterval(UpdateTime,1000);
//clearInterval(t);

function TransformColorToRGB(color){
	//0  >255   : R=255 		+ G=VALUE			+ B=0				#FF0000 to #FFFF00
	if (color.all<255) {
		color.colorR=255;
		color.colorG=color;
		color.colorB=0;
	}
	//256>511   : R=511-VALUE	+ G=255 			+ B=0				#FFFF00 to #00FF00
	else if (color.all<510) {
		color.colorR=510-color.all;
		color.colorG=255;
		color.colorB=0;
	}
	//512>767   : R=0			+ G=255				+ B=VALUE-512		#00FF00 to #00FFFF
	else if (color.all<765) {
		color.colorR=0;
		color.colorG=255;
		color.colorB=color.all-510;
	}
	//768>1023  : R=0			+ G=1023-VALUE		+ B=255				#00FFFF to #0000FF
	else if (color.all<1020) {
		color.colorR=0;
		color.colorG=1020-color.all;
		color.colorB=255;
	}
	//1024>1279 : R=VALUE-1024	+ G=0				+ B=255				#0000FF to #FF00FF
	else if (color.all<1275) {
		color.colorR=color.all-1020;
		color.colorG=0;
		color.colorB=255;
	}
	//1280>1535 : R=255			+ G=0				+ B=1535-VALUE		#FF00FF to #FF0000
	else if (color.all<1530) {
		color.colorR=255;
		color.colorG=0;
		color.colorB=1530-color.all;
	}
}

//Add websocket
var gateway = `ws://${window.location.hostname}/ws`;
var websocket;
window.addEventListener('load', onLoad);
function initWebSocket() {
	console.log('Trying to open a WebSocket connection...');
	websocket = new WebSocket(gateway);
	websocket.onopen    = onOpen;
	websocket.onclose   = onClose;
	websocket.onmessage = onMessage; // <-- add this line
}
function onOpen(event) {
	console.log('Connection opened');
}
function onClose(event) {
	console.log('Connection closed');
	setTimeout(initWebSocket, 2000);
}
function onMessage(event) {
	var stringReceived;
	stringReceived=JSON.parse(String(event.data));
	console.log("onMessage="+JSON.stringify(stringReceived));
	if (stringReceived.hasOwnProperty("timeout") == true) {
		document.getElementById("timeout").value=stringReceived["timeout"];
		document.getElementById("timeoutValid").innerHTML="OK";
	}
	if (stringReceived.hasOwnProperty("delay") == true) {
		document.getElementById("pwmSlider_Delay").value = stringReceived["delay"];
	}
	if (stringReceived.hasOwnProperty("color") == true) {
		document.getElementById("pwmSlider_"+stringReceived["color"]).value = parseInt(stringReceived[stringReceived["color"]]);
	} else {
		if (stringReceived.hasOwnProperty("colorRed") == true) {
			document.getElementById("pwmSlider_Red").value = parseInt(stringReceived["colorRed"]);
		}
		if (stringReceived.hasOwnProperty("colorGreen") == true) {
			document.getElementById("pwmSlider_Green").value = parseInt(stringReceived["colorGreen"]);
		}
		if (stringReceived.hasOwnProperty("colorBlue") == true) {
			document.getElementById("pwmSlider_Blue").value = parseInt(stringReceived["colorBlue"]);
		}
	}
	if (stringReceived.hasOwnProperty("type") == true) {
		document.getElementById("pwmSlider_"+stringReceived["type"]).value = parseInt(stringReceived[stringReceived["type"]]);
		if (stringReceived["type"] == "Color") {
			var color = {all: stringReceived["Color"], colorR: 0, colorG:0, colorB:0};
			TransformColorToRGB(color);
			document.getElementById("pwmSlider_Red").value		= color.colorR;
			document.getElementById("pwmSlider_Green").value	= color.colorG;
			document.getElementById("pwmSlider_Blue").value		= color.colorB;
		}
	} else {
		if (stringReceived.hasOwnProperty("Color") == true) {
			document.getElementById("pwmSlider_Color").value = parseInt(stringReceived["Color"]);
		}
		if (stringReceived.hasOwnProperty("Brightness") == true) {
			document.getElementById("pwmSlider_Brightness").value = parseInt(stringReceived["Brightness"]);
		}
	}
	if (stringReceived.hasOwnProperty("idMode") == true) {
		document.getElementById("pwmSlider_Brightness").innerHTML = buttonToAnimationName(stringReceived["idMode"]);
	}
}
function onLoad(event) {
	initWebSocket();
}

</script>
</body>
</html>
)rawliteral";


byte position=0;	//Current position selected 1:North, 2:Middle, 4:South, 8:Ext, 15:Full
byte buttonClicked=0;				//Button clicked on HTML
byte animation[4]={0,0,0,0};		//0:No animation, 1:Fade3, 2:Fade7, 3:Jump3, 4:Jump7, [0]:North…

int animationDelay[4]={100,100,100,100};	//Delay for transition
//Last color set on slider, which will be used when init new page
byte lastDelay;								//Last delay updated, used for html slider view
unsigned int lastColor;						//Last color from sliderColor
byte lastBrightness;				//Last brightness from sliderBrightness
byte lastColorR;					//Last color from sliderRed
byte lastColorG;					//Last color from sliderGreen
byte lastColorB;					//Last color from sliderBlue
byte lastAnimation;					//Last animation chosen from button

byte color[4][3]={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};		//position, colors RGB
byte brightness[4]={255,255,255,255}, oldBrightness[4];	//255:Max, 0:Off

unsigned long tickDelayOff=0;	//Seconds before turning off

void setup(){
	// Serial port for communicating with others arduino
	Serial.begin(115200);
	irrecv2.enableIRIn(); // Start the receiver

	ArduinoOTA.setHostname("Plafond");
	ArduinoOTA.setPassword("plafond");
	ArduinoOTA.onStart([]() {
		//Start OTA
	});
	ArduinoOTA.onEnd([]() {
		//End of OTA
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
#if DBG_UART==1
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
#endif
	});
	ArduinoOTA.onError([](ota_error_t error) {
#if DBG_UART==1
		if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
		else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
		else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
		else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
		else if (error == OTA_END_ERROR) Serial.println("End Failed");
#endif
	});
	ArduinoOTA.begin();

//	analogWriteFreq(8000);
	analogWriteRange(4095);

	pinMode(Do_LED0_R, OUTPUT);
	pinMode(Do_LED0_G, OUTPUT);
	pinMode(Do_LED0_B, OUTPUT);
	pinMode(Do_LED1_R, OUTPUT);
	pinMode(Do_LED1_G, OUTPUT);
	pinMode(Do_LED1_B, OUTPUT);
	pinMode(Do_DUMMY, OUTPUT);

	analogWrite(Do_DUMMY, 4094);	//Min to don't call pwm start
	analogWrite(Do_LED0_R, 0);
	analogWrite(Do_LED0_G, 0);
	analogWrite(Do_LED0_B, 0);
	analogWrite(Do_LED1_R, 0);
	analogWrite(Do_LED1_G, 0);
	analogWrite(Do_LED1_B, 0);

	// Connect to Wi-Fi
	IPAddress subnet(255, 255, 255, 0); // set subnet mask to match your
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(ssid, password);
#if DBG_UART==1
	Serial.print("Connecting to WiFi");
#endif
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
#if DBG_UART==1
		Serial.print(".");
#endif
	}

#if DBG_UART==1
	// Print ESP Local IP Address
	Serial.println(WiFi.localIP());
#endif

	initWebSocket();

	// Route for root / web page
	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send_P(200, "text/html", index_html);
	});

	// Send a GET request to <ESP_IP>/slider?value=<inputMessage>
	server.on("/slider", HTTP_GET, [] (AsyncWebServerRequest *request) {
		// GET input value on <ESP_IP>/slider?position=<position>&color=<color> or brightness="brightness"
		unsigned int l_color=0;
		if (request->hasParam("position")) {	//Get position
			position = request->getParam("position")->value().toInt();
		}
		if (request->hasParam("brightness")) {	//Get position
			SetArrayByte(position, brightness, request->getParam("brightness")->value().toInt());
			lastBrightness=request->getParam("brightness")->value().toInt();
		}
		if (request->hasParam("color")) {	//Get position
			l_color = request->getParam("color")->value().toInt();
			SetFullColor(position, l_color);
			SetArrayByte(position, animation, 0);
			lastColor = l_color;
		}
		if (request->hasParam("r") && request->hasParam("g") && request->hasParam("b")) {
			SetColor(position, request->getParam("r")->value().toInt(), request->getParam("g")->value().toInt(), request->getParam("b")->value().toInt());
			SetArrayByte(position, animation, 0);
			lastColorR = request->getParam("r")->value().toInt();
			lastColorG = request->getParam("g")->value().toInt();
			lastColorB = request->getParam("b")->value().toInt();
		}
		request->send(200, "text/plain", "OK");
	});


	// GET request to <ESP_IP>/delayOff?minutes=<inputMessage>
	server.on("/delayOff", HTTP_GET, [] (AsyncWebServerRequest *request) {
		// GET input value on <ESP_IP>/delayOff?minutes=<minutes>
		if (request->hasParam("seconds")) {	//Get position
			tickDelayOff = millis()+request->getParam("seconds")->value().toInt()*1000;
			if(tickDelayOff==0) tickDelayOff=1;
		}
		request->send(200, "text/plain", "OK");
	});

	// Send a GET request to <ESP_IP>/slider?value=<inputMessage>
	server.on("/button", HTTP_GET, [] (AsyncWebServerRequest *request) {
		// GET input1 value on <ESP_IP>/slider?value=<inputMessage>
		if (request->hasParam("value")) {	//Button clicked
			buttonClicked=request->getParam("value")->value().toInt();
			lastAnimation=buttonClicked;
#if DBG_UART==1
			Serial.print("Click on : ");
			Serial.println(buttonClicked);
#endif
		}
		if (request->hasParam("position")) {	//Button clicked
			position=request->getParam("position")->value().toInt();
#if DBG_UART==1
			Serial.print("position : ");
			Serial.println(position);
#endif
		}
		if (request->hasParam("delay")) {	//Button clicked
			SetDelay(position, request->getParam("delay")->value().toInt());
#if DBG_UART==1
			Serial.print("delay : ");
			Serial.println(animationDelay[position]);
#endif
		}
		request->send(200, "text/plain", "OK");
	});

	// Start server
	server.begin();
}

void loop() {
	static byte oldColor[4][3];
	static char dataToSend[] = "D.......";

	//OTA
	ArduinoOTA.handle();

	//WS
	ws.cleanupClients();

	if (irrecv2.decode()) {	//IR decoded
		uint32_t value=irrecv2.decodedIRData.decodedRawData;
		if (value!=0) {
			if (value==IR_VP) {
			} else if (value==IR_VM) {
			} else if (value==IR_NEXT) {
				position=15;
			} else if (value==IR_MUSIC1) {
				position=1;
			} else if (value==IR_MUSIC2) {
				position=2;
			} else if (value==IR_MUSIC3) {
				position=4;
			} else if (value==IR_MUSIC4) {
				position=8;
			} else if (value==IR_RED) {
				SetColor(position, 255, 0, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_GREEN) {
				SetColor(position, 0, 255, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_BLUE) {
				SetColor(position, 0, 0, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_WHITE) {
				SetColor(position, 255, 255, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_ORANGE1) {
				SetColor(position, 255, 64, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_ORANGE2) {
				SetColor(position, 255, 128, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_ORANGE3) {
				SetColor(position, 255, 191, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_YELLOW) {
				SetColor(position, 200, 255, 0);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_GREEN1) {
				SetColor(position, 0, 255, 64);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_CYAN1) {
				SetColor(position, 0, 255, 128);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_CYAN2) {
				SetColor(position, 0, 255, 191);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_CYAN3) {
				SetColor(position, 0, 255, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_BLUE1) {
				SetColor(position, 20, 20, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_PURPLE1) {
				SetColor(position, 85, 0, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_PURPLE2) {
				SetColor(position, 170, 0, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_PINK) {
				SetColor(position, 255, 0, 255);
				SetArrayByte(position, animation, 0);
			} else if (value==IR_LIGHT1) {
				SetArrayByte(position, brightness, 63);
			} else if (value==IR_LIGHT2) {
				SetArrayByte(position, brightness, 128);
			} else if (value==IR_LIGHT3) {
				SetArrayByte(position, brightness, 191);
			} else if (value==IR_LIGHT4) {
				SetArrayByte(position, brightness, 255);
			} else if (value==IR_FADE3) {
				SetArrayByte(position, animation, 1);
			} else if (value==IR_FADE7) {
				SetArrayByte(position, animation, 2);
			} else if (value==IR_JUMP3) {
				SetArrayByte(position, animation, 3);
			} else if (value==IR_JUMP7) {
				SetArrayByte(position, animation, 4);
			}
		}
		irrecv2.resume(); // Receive the next value
	}

	static unsigned int counterFade3[4]={0,0,0,0};
	static unsigned int counterFade7[4]={0,0,0,0};
	static byte counterJump3[4]={0,0,0,0};
	static byte counterJump7[4]={0,0,0,0};
	//Check button status
	if (buttonClicked==ANIM_FADE3) {						//Fade3
		SetArrayByte(position, animation, ANIM_FADE3);
		SetArrayUint(position, counterFade3, 0);
	} else if (buttonClicked==ANIM_FADE7) {				//Fade7
		SetArrayByte(position, animation, ANIM_FADE7);
		SetArrayUint(position, counterFade7, 0);
	} else if (buttonClicked==ANIM_JUMP3) {				//Jump3
		SetArrayByte(position, animation, ANIM_JUMP3);
		SetArrayByte(position, counterJump3, 0);
	} else if (buttonClicked==ANIM_JUMP7) {				//Jump7
		SetArrayByte(position, animation, ANIM_JUMP7);
		SetArrayByte(position, counterJump7, 0);
	} else if (buttonClicked==ANIM_STOP) {				//Stop
		SetArrayByte(position, animation, 0);
		SetColor(position, 0,0,0);
	} else if (buttonClicked==ANIM_WHITE) {				//Full white
		SetColor(position, 255, 255, 255);
		SetArrayByte(position, animation, 0);
	} else if (buttonClicked==ANIM_FIRE) {				//Fire
		SetArrayByte(position, animation, ANIM_FIRE);
		SetColor(position, 230, 100, 0);
		SetDelay(position, 25);
	} else if (buttonClicked==ANIM_JUNGLE) {				//Jungle
		SetArrayByte(position, animation, ANIM_JUNGLE);
		SetColor(position, 60, 230, 0);
		SetDelay(position, 25);
	} else if (buttonClicked==ANIM_WATER) {				//Water
		SetArrayByte(position, animation, ANIM_WATER);
		SetColor(position, 0, 230, 230);
		SetDelay(position, 25);
	} else if (buttonClicked==ANIM_PINKY) {				//Pinky
		SetArrayByte(position, animation, ANIM_PINKY);
		SetColor(position, 230, 0, 120);
		SetDelay(position, 25);
	} else if (buttonClicked==ANIM_AUTOFADE) {				//AutoFade
		SetArrayByte(15, animation, ANIM_FADE7);
		SetDelay(5, 26);
		SetDelay(2, 22);
		SetDelay(8, 35);
		SetArrayByte(position, brightness, 100);
	} else if (buttonClicked==ANIM_DISCO) {				//Disco
		SetArrayByte(15, animation, ANIM_JUMP7);
		SetDelay(5, 26);
		SetDelay(2, 22);
		SetDelay(8, 35);
		SetArrayByte(position, brightness, 100);
	} else if (buttonClicked==ANIM_EPILEPTIQUE) {				//Epileptique
		SetArrayByte(8, animation, ANIM_JUMP3);
		SetArrayByte(7, animation, ANIM_JUMP7);
		SetDelay(5, 7);
		SetDelay(2, 3);
		SetDelay(8, 15);
		SetArrayByte(position, brightness, 100);
	} else if (buttonClicked==ANIM_TV) {				//Fix yellow
		SetArrayByte(15, animation, 0);
		SetArrayByte(5, brightness, 80);
		SetFullColor(5, 95);
		SetArrayByte(10, brightness, 150);
		SetFullColor(10, 160);
	}
	buttonClicked=0;

	if(tickDelayOff!=0 && millis()>tickDelayOff){
		//Need to turn off when millis > tickDelayOff
		static unsigned long reduceBrightness=0;
		if(millis()-reduceBrightness>DELAY_REDUCE_BRIGHTNESS){
			reduceBrightness = millis();
			for (byte i = 0; i < 4; i++) {
				if(brightness[i]>0){
					brightness[i]--;
				}
			}
			if (brightness[0]==0 && brightness[1]==0 && brightness[2]==0 && brightness[3]==0) {
				tickDelayOff = 0;
			}
		}
	}

	//Animation
	static unsigned long tick_Animation[4];
	byte counter;
	for (counter = 0; counter < 4; counter += 1) {
		unsigned int counterStep=1;
		if (animationDelay[counter]<0) {
			counterStep=-animationDelay[counter];
		}
		if (animation[counter]==ANIM_FADE3) {	//Fade3
			if ((animationDelay[counter]<0) || (millis()-tick_Animation[counter]>=animationDelay[counter])) {
				tick_Animation[counter]=millis();
				counterFade3[counter]=(counterFade3[counter]+counterStep)%765;
				SetHalfColor((1<<counter), counterFade3[counter]);
			}
		} else if (animation[counter]==ANIM_FADE7) {	//Fade7
			if ((animationDelay[counter]<0) || (millis()-tick_Animation[counter]>=animationDelay[counter])) {
				tick_Animation[counter]=millis();
				counterFade7[counter]=(counterFade7[counter]+counterStep)%1530;
				SetFullColor((1<<counter), counterFade7[counter]);
			}
		} else if (animation[counter]==ANIM_JUMP3) {	//Jump3
			if ((millis()-tick_Animation[counter]>=600+30*animationDelay[counter])) {
				tick_Animation[counter]=millis();
				counterJump3[counter]=(counterJump3[counter]+1)%3;
				SetColor((1<<counter), (counterJump3[counter]==0)*255, (counterJump3[counter]==1)*255, (counterJump3[counter]==2)*255);
			}
		} else if (animation[counter]==ANIM_JUMP7) {	//Jump7
			if ((millis()-tick_Animation[counter]>=600+30*animationDelay[counter])) {
				tick_Animation[counter]=millis();
				if     (counterJump7[counter]==1) counterJump7[counter] = 3;
				else if(counterJump7[counter]==3) counterJump7[counter] = 2;
				else if(counterJump7[counter]==2) counterJump7[counter] = 6;
				else if(counterJump7[counter]==6) counterJump7[counter] = 4;
				else if(counterJump7[counter]==4) counterJump7[counter] = 5;
				else                              counterJump7[counter] = 1;
				SetColor((1<<counter), ((counterJump7[counter]&1)==1)*255, ((counterJump7[counter]&2)==2)*255, ((counterJump7[counter]&4)==4)*255);
			}
		} else if (animation[counter]==ANIM_FIRE && (millis()-tick_Animation[counter]>=20+animationDelay[counter])) {
			tick_Animation[counter]=millis();
			color[counter][COLOR_R]+=SetRandColor(color[counter][COLOR_R], 200, 255, -3, 3);
			color[counter][COLOR_G]+=SetRandColor(color[counter][COLOR_G], 80, 150, -3, 3);
			color[counter][COLOR_B]=0;
		} else if (animation[counter]==ANIM_JUNGLE && (millis()-tick_Animation[counter]>=20+animationDelay[counter])) {
			tick_Animation[counter]=millis();
			color[counter][COLOR_R]+=SetRandColor(color[counter][COLOR_R], 10, 80, -5, 5);
			color[counter][COLOR_G]+=SetRandColor(color[counter][COLOR_G], 200, 255, -3, 3);
			color[counter][COLOR_B]=0;
		} else if (animation[counter]==ANIM_WATER && (millis()-tick_Animation[counter]>=20+animationDelay[counter])) {
			tick_Animation[counter]=millis();
			color[counter][COLOR_R]=0;
			color[counter][COLOR_G]+=SetRandColor(color[counter][COLOR_G], 200, 255, -3, 3);
			color[counter][COLOR_B]+=SetRandColor(color[counter][COLOR_B], 200, 255, -3, 3);
		} else if (animation[counter]==ANIM_PINKY && (millis()-tick_Animation[counter]>=20+animationDelay[counter])) {
			tick_Animation[counter]=millis();
			color[counter][COLOR_R]+=SetRandColor(color[counter][COLOR_R], 200, 255, -5, 5);
			color[counter][COLOR_G]=0;
			color[counter][COLOR_B]+=SetRandColor(color[counter][COLOR_B], 80, 160, -3, 3);
		}
	}

	//Update LED :
	//For nano:
	if (color[POS_NORTH][COLOR_R]!=oldColor[POS_NORTH][COLOR_R] || brightness[POS_NORTH]!=oldBrightness[POS_NORTH]) {
		strcpy(dataToSend+1, String(POS_NORTH).c_str());
		strcpy(dataToSend+2, String("R").c_str());
		strcpy(dataToSend+3, String((color[POS_NORTH][COLOR_R]*brightness[POS_NORTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_NORTH][COLOR_R]=color[POS_NORTH][COLOR_R];
	}
	if (color[POS_NORTH][COLOR_G]!=oldColor[POS_NORTH][COLOR_G] || brightness[POS_NORTH]!=oldBrightness[POS_NORTH]) {
		strcpy(dataToSend+1, String(POS_NORTH).c_str());
		strcpy(dataToSend+2, String("G").c_str());
		strcpy(dataToSend+3, String((color[POS_NORTH][COLOR_G]*brightness[POS_NORTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_NORTH][COLOR_G]=color[POS_NORTH][COLOR_G];
	}
	if (color[POS_NORTH][COLOR_B]!=oldColor[POS_NORTH][COLOR_B] || brightness[POS_NORTH]!=oldBrightness[POS_NORTH]) {
		strcpy(dataToSend+1, String(POS_NORTH).c_str());
		strcpy(dataToSend+2, String("B").c_str());
		strcpy(dataToSend+3, String((color[POS_NORTH][COLOR_B]*brightness[POS_NORTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_NORTH][COLOR_B]=color[POS_NORTH][COLOR_B];
	}
	if (color[POS_SOUTH][COLOR_R]!=oldColor[POS_SOUTH][COLOR_R] || brightness[POS_SOUTH]!=oldBrightness[POS_SOUTH]) {
		strcpy(dataToSend+1, String(POS_SOUTH).c_str());
		strcpy(dataToSend+2, String("R").c_str());
		strcpy(dataToSend+3, String((color[POS_SOUTH][COLOR_R]*brightness[POS_SOUTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_SOUTH][COLOR_R]=color[POS_SOUTH][COLOR_R];
	}
	if (color[POS_SOUTH][COLOR_G]!=oldColor[POS_SOUTH][COLOR_G] || brightness[POS_SOUTH]!=oldBrightness[POS_SOUTH]) {
		strcpy(dataToSend+1, String(POS_SOUTH).c_str());
		strcpy(dataToSend+2, String("G").c_str());
		strcpy(dataToSend+3, String((color[POS_SOUTH][COLOR_G]*brightness[POS_SOUTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_SOUTH][COLOR_G]=color[POS_SOUTH][COLOR_G];
	}
	if (color[POS_SOUTH][COLOR_B]!=oldColor[POS_SOUTH][COLOR_B] || brightness[POS_SOUTH]!=oldBrightness[POS_SOUTH]) {
		strcpy(dataToSend+1, String(POS_SOUTH).c_str());
		strcpy(dataToSend+2, String("B").c_str());
		strcpy(dataToSend+3, String((color[POS_SOUTH][COLOR_B]*brightness[POS_SOUTH]+128)/255).c_str());
		Serial.println(dataToSend);
		oldColor[POS_SOUTH][COLOR_B]=color[POS_SOUTH][COLOR_B];
	}
	//For wemos
	if (color[POS_MIDDLE][COLOR_R]!=oldColor[POS_MIDDLE][COLOR_R] || brightness[POS_MIDDLE]!=oldBrightness[POS_MIDDLE]) {
		analogWrite(Do_LED0_R, correctPWM[(color[POS_MIDDLE][COLOR_R]*brightness[POS_MIDDLE]+128)/255]);
		oldColor[POS_MIDDLE][COLOR_R]=color[POS_MIDDLE][COLOR_R];
	}
	if (color[POS_MIDDLE][COLOR_G]!=oldColor[POS_MIDDLE][COLOR_G] || brightness[POS_MIDDLE]!=oldBrightness[POS_MIDDLE]) {
		analogWrite(Do_LED0_G, correctPWM[(color[POS_MIDDLE][COLOR_G]*brightness[POS_MIDDLE]+128)/255]);
		oldColor[POS_MIDDLE][COLOR_G]=color[POS_MIDDLE][COLOR_G];
	}
	if (color[POS_MIDDLE][COLOR_B]!=oldColor[POS_MIDDLE][COLOR_B] || brightness[POS_MIDDLE]!=oldBrightness[POS_MIDDLE]) {
		analogWrite(Do_LED0_B, correctPWM[(color[POS_MIDDLE][COLOR_B]*brightness[POS_MIDDLE]+128)/255]);
		oldColor[POS_MIDDLE][COLOR_B]=color[POS_MIDDLE][COLOR_B];
	}

	if (color[POS_EXT][COLOR_R]!=oldColor[POS_EXT][COLOR_R] || brightness[POS_EXT]!=oldBrightness[POS_EXT]) {
		analogWrite(Do_LED1_R, correctPWM[(color[POS_EXT][COLOR_R]*brightness[POS_EXT]+128)/255]);
		oldColor[POS_EXT][COLOR_R]=color[POS_EXT][COLOR_R];
	}
	if (color[POS_EXT][COLOR_G]!=oldColor[POS_EXT][COLOR_G] || brightness[POS_EXT]!=oldBrightness[POS_EXT]) {
		analogWrite(Do_LED1_G, correctPWM[(color[POS_EXT][COLOR_G]*brightness[POS_EXT]+128)/255]);
		oldColor[POS_EXT][COLOR_G]=color[POS_EXT][COLOR_G];
	}
	if (color[POS_EXT][COLOR_B]!=oldColor[POS_EXT][COLOR_B] || brightness[POS_EXT]!=oldBrightness[POS_EXT]) {
		analogWrite(Do_LED1_B, correctPWM[(color[POS_EXT][COLOR_B]*brightness[POS_EXT]+128)/255]);
		oldColor[POS_EXT][COLOR_B]=color[POS_EXT][COLOR_B];
	}

	oldBrightness[POS_NORTH]=brightness[POS_NORTH];
	oldBrightness[POS_SOUTH]=brightness[POS_SOUTH];
	oldBrightness[POS_MIDDLE]=brightness[POS_MIDDLE];
	oldBrightness[POS_EXT]=brightness[POS_EXT];

	delay(1);
}

void SetColor(byte position, byte colorR, byte colorG, byte colorB){
#if DBG_UART==1
	Serial.print(position);
	Serial.print(":R=");
	Serial.print(colorR);
	Serial.print(",G=");
	Serial.print(colorG);
	Serial.print(",B=");
	Serial.println(colorB);
#endif
	if ((position&1)==1) {
		color[POS_NORTH][COLOR_R]=colorR;
		color[POS_NORTH][COLOR_G]=colorG;
		color[POS_NORTH][COLOR_B]=colorB;
	}
	if ((position&2)==2) {
		color[POS_MIDDLE][COLOR_R]=colorR;
		color[POS_MIDDLE][COLOR_G]=colorG;
		color[POS_MIDDLE][COLOR_B]=colorB;
	}
	if ((position&4)==4) {
		color[POS_SOUTH][COLOR_R]=colorR;
		color[POS_SOUTH][COLOR_G]=colorG;
		color[POS_SOUTH][COLOR_B]=colorB;
	}
	if ((position&8)==8) {
		color[POS_EXT][COLOR_R]=colorR;
		color[POS_EXT][COLOR_G]=colorG;
		color[POS_EXT][COLOR_B]=colorB;
	}
}


void SetArrayUint(byte position, unsigned int array[4], byte l_value){
	if ((position&1)==1) {
		array[POS_NORTH]=l_value;
	}
	if ((position&2)==2) {
		array[POS_MIDDLE]=l_value;
	}
	if ((position&4)==4) {
		array[POS_SOUTH]=l_value;
	}
	if ((position&8)==8) {
		array[POS_EXT]=l_value;
	}
}

void SetArrayByte(byte position, byte array[4], byte l_value){
	
	if ((position&1)==1) {
		array[POS_NORTH]=l_value;
	}
	if ((position&2)==2) {
		array[POS_MIDDLE]=l_value;
	}
	if ((position&4)==4) {
		array[POS_SOUTH]=l_value;
	}
	if ((position&8)==8) {
		array[POS_EXT]=l_value;
	}
}

void SetDelay(byte position, byte l_delay){
	lastDelay = l_delay;
	if ((position&1)==1) {
		animationDelay[POS_NORTH]=correctDelay[l_delay];
	}
	if ((position&2)==2) {
		animationDelay[POS_MIDDLE]=correctDelay[l_delay];
	}
	if ((position&4)==4) {
		animationDelay[POS_SOUTH]=correctDelay[l_delay];
	}
	if ((position&8)==8) {
		animationDelay[POS_EXT]=correctDelay[l_delay];
	}
}


void SetFullColor(byte l_position, unsigned int l_value){
	//0  >255   : R=255 		+ G=VALUE			+ B=0				#FF0000 to #FFFF00
	if (l_value<255) {
		SetColor(l_position, 255, l_value, 0);
	}
	//256>511   : R=511-VALUE	+ G=255 			+ B=0				#FFFF00 to #00FF00
	else if (l_value<510) {
		SetColor(l_position, 510-l_value, 255, 0);
	}
	//512>767   : R=0			+ G=255				+ B=VALUE-512		#00FF00 to #00FFFF
	else if (l_value<765) {
		SetColor(l_position, 0, 255, l_value-510);
	}
	//768>1023  : R=0			+ G=1023-VALUE		+ B=255				#00FFFF to #0000FF
	else if (l_value<1020) {
		SetColor(l_position, 0, 1020-l_value, 255);
	}
	//1024>1279 : R=VALUE-1024	+ G=0				+ B=255				#0000FF to #FF00FF
	else if (l_value<1275) {
		SetColor(l_position, l_value-1020, 0, 255);
	}
	//1280>1535 : R=255			+ G=0				+ B=1535-VALUE		#FF00FF to #FF0000
	else if (l_value<1530) {
		SetColor(l_position, 255, 0, 1530-l_value);
	}
}

void SetHalfColor(byte l_position, unsigned int l_value){
	//0  >255   : R=255 		+ G=VALUE			+ B=0				#FF0000 to #FFFF00
	if (l_value<255) {
		SetColor(l_position, 255-l_value, l_value, 0);
	}
	//256>511   : R=511-VALUE	+ G=255 			+ B=0				#FFFF00 to #00FF00
	else if (l_value<510) {
		SetColor(l_position, 0, 510-l_value, l_value-255);
	}
	//512>767   : R=0			+ G=255				+ B=VALUE-512		#00FF00 to #00FFFF
	else if (l_value<765) {
		SetColor(l_position, l_value-510, 0, 765-l_value);
	}
}

int SetRandColor(byte currentColor, byte limitMin, byte limitMax, int randMin, byte randMax){
	byte ret=0;
	if (currentColor>limitMin+randMax && currentColor<limitMax-randMax) {
		ret=(random(randMax-randMin+1)+randMin);
	} else if (currentColor<=limitMin+randMax) {
		ret=(random(randMax+1));
	} else if (currentColor>=limitMax-randMax) {
		ret=-(random(-randMin+1));
	}
	return ret;
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
	AwsFrameInfo *info = (AwsFrameInfo*)arg;
	if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {
		data[len]=0;
		ws.textAll(String((char*) data));	//notifyClients to websocket.onmessage function
		
		/*
		//From https://arduinojson.org/v6/assistant/
		StaticJsonDocument<1024> jsData;
		DeserializationError error = deserializeJson(jsData, input, inputLength);
		if (error) {
			Serial.print(F("deserializeJson() failed: "));
			Serial.println(error.f_str());
			return;
		}
		const char* timeout = jsData["timeout"]; // "22:06"
		*/
	}
}

//Init html with current values
void InitHtmlData(){
	String str_dataToSend;
	StaticJsonDocument<1024> jsInitData;
	jsInitData["timeout"] = "22:06";
	jsInitData["delay"]=lastDelay;
	jsInitData["Color"]=lastColor;
	jsInitData["colorRed"]=lastColorR;
	jsInitData["colorGreen"]=lastColorG;
	jsInitData["colorBlue"]=lastColorB;
	jsInitData["idMode"]=lastAnimation;
	jsInitData["Brightness"]=lastBrightness;
	
	serializeJson(jsInitData, str_dataToSend);
	ws.textAll(str_dataToSend);	//notifyClients to websocket.onmessage function
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
	switch (type) {
	  case WS_EVT_CONNECT:
#if DBG_UART==1
		Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
#endif
		InitHtmlData();
		break;
#if DBG_UART==1
	  case WS_EVT_DISCONNECT:
		Serial.printf("WebSocket client #%u disconnected\n", client->id());
		break;
#endif
	  case WS_EVT_DATA:
#if DBG_UART==1
		Serial.printf("client #%u : %s\n", client->id(), data);
#endif
		handleWebSocketMessage(arg, data, len);
		break;
	  case WS_EVT_PONG:
	  case WS_EVT_ERROR:
		break;
	}
}

void initWebSocket() {
	ws.onEvent(onEvent);
	server.addHandler(&ws);
}

