EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:WeMos_D1_mini U1
U 1 1 61757C35
P 2100 2500
F 0 "U1" H 2300 1850 50  0000 C CNN
F 1 "WeMos_D1_mini" H 2050 2050 50  0000 C CNN
F 2 "Module:WEMOS_D1_mini_light" H 2100 1350 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H 250 1350 50  0001 C CNN
	1    2100 2500
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 617589FC
P 5800 2450
F 0 "A1" H 5800 2450 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 5850 2800 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 5800 2450 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5800 2450 50  0001 C CNN
	1    5800 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 6175994E
P 4300 4300
F 0 "J1" H 4328 4321 50  0000 L CNN
F 1 "691411410002" H 4328 4230 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-2P_691411410002" H 4300 4300 50  0001 C CNN
F 3 "~" H 4300 4300 50  0001 C CNN
F 4 "12V" H 4328 4139 50  0000 L CNN "Info"
	1    4300 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 6175A3BF
P 4400 6100
F 0 "J4" H 4428 6121 50  0000 L CNN
F 1 "691411410004" H 4428 6030 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-4P_691411410004" H 4400 6100 50  0001 C CNN
F 3 "~" H 4400 6100 50  0001 C CNN
F 4 "LED1" H 4428 5939 50  0000 L CNN "Info"
	1    4400 6100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 6175A85B
P 4300 4700
F 0 "J2" H 4328 4721 50  0000 L CNN
F 1 "691411410002" H 4328 4630 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-2P_691411410002" H 4300 4700 50  0001 C CNN
F 3 "~" H 4300 4700 50  0001 C CNN
F 4 "GND" H 4328 4539 50  0000 L CNN "Info"
	1    4300 4700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J3
U 1 1 6175AE46
P 4400 5500
F 0 "J3" H 4428 5521 50  0000 L CNN
F 1 "691411410004" H 4428 5430 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-4P_691411410004" H 4400 5500 50  0001 C CNN
F 3 "~" H 4400 5500 50  0001 C CNN
F 4 "LED0" H 4428 5339 50  0000 L CNN "Info"
	1    4400 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J5
U 1 1 6175B1C7
P 4400 6700
F 0 "J5" H 4428 6721 50  0000 L CNN
F 1 "691411410004" H 4428 6630 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-4P_691411410004" H 4400 6700 50  0001 C CNN
F 3 "~" H 4400 6700 50  0001 C CNN
F 4 "LED2" H 4428 6539 50  0000 L CNN "Info"
	1    4400 6700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J6
U 1 1 6175B5AD
P 4400 7300
F 0 "J6" H 4428 7321 50  0000 L CNN
F 1 "691411410004" H 4428 7230 50  0000 L CNN
F 2 "Wurth:WR-TBL-7.62mm-4P_691411410004" H 4400 7300 50  0001 C CNN
F 3 "~" H 4400 7300 50  0001 C CNN
F 4 "LED3" H 4428 7139 50  0000 L CNN "Info"
	1    4400 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 6175D5B2
P 7600 1100
F 0 "Q1" H 7805 1146 50  0000 L CNN
F 1 "AP4438" H 7805 1055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 7800 1200 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 7600 1100 50  0001 C CNN
F 4 "C353072" H 7600 1100 50  0001 C CNN "LCSC"
	1    7600 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 6175F832
P 7250 1100
F 0 "R1" V 7150 1100 50  0000 C CNN
F 1 "1k" V 7250 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 1100 50  0001 C CNN
F 3 "~" H 7250 1100 50  0001 C CNN
	1    7250 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 6175FC57
P 7400 1250
F 0 "R4" V 7300 1250 50  0000 C CNN
F 1 "470k" V 7400 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7330 1250 50  0001 C CNN
F 3 "~" H 7400 1250 50  0001 C CNN
	1    7400 1250
	-1   0    0    1   
$EndComp
Connection ~ 7400 1100
Wire Wire Line
	7700 1300 7700 1400
Wire Wire Line
	7700 1400 7600 1400
$Comp
L power:GNDPWR #PWR03
U 1 1 6176076F
P 7600 1400
F 0 "#PWR03" H 7600 1200 50  0001 C CNN
F 1 "GNDPWR" H 7604 1246 50  0000 C CNN
F 2 "" H 7600 1350 50  0001 C CNN
F 3 "" H 7600 1350 50  0001 C CNN
	1    7600 1400
	1    0    0    -1  
$EndComp
Connection ~ 7600 1400
Wire Wire Line
	7600 1400 7400 1400
Wire Wire Line
	4100 4400 4100 4300
$Comp
L power:+12V #PWR021
U 1 1 6176132E
P 4100 4300
F 0 "#PWR021" H 4100 4150 50  0001 C CNN
F 1 "+12V" H 4115 4473 50  0000 C CNN
F 2 "" H 4100 4300 50  0001 C CNN
F 3 "" H 4100 4300 50  0001 C CNN
	1    4100 4300
	1    0    0    -1  
$EndComp
Connection ~ 4100 4300
Wire Wire Line
	4100 4700 4100 4800
$Comp
L power:GNDPWR #PWR022
U 1 1 61761B07
P 4100 4800
F 0 "#PWR022" H 4100 4600 50  0001 C CNN
F 1 "GNDPWR" H 4104 4646 50  0000 C CNN
F 2 "" H 4100 4750 50  0001 C CNN
F 3 "" H 4100 4750 50  0001 C CNN
	1    4100 4800
	1    0    0    -1  
$EndComp
Connection ~ 4100 4800
$Comp
L Regulator_Linear:L7805 U2
U 1 1 61762336
P 2050 5000
F 0 "U2" H 2050 5242 50  0000 C CNN
F 1 "L7805" H 2050 5151 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2075 4850 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 2050 4950 50  0001 C CNN
	1    2050 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR024
U 1 1 61762BAC
P 1750 5000
F 0 "#PWR024" H 1750 4850 50  0001 C CNN
F 1 "+12V" H 1765 5173 50  0000 C CNN
F 2 "" H 1750 5000 50  0001 C CNN
F 3 "" H 1750 5000 50  0001 C CNN
	1    1750 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR028
U 1 1 61762FA5
P 2050 5300
F 0 "#PWR028" H 2050 5100 50  0001 C CNN
F 1 "GNDPWR" H 2054 5146 50  0000 C CNN
F 2 "" H 2050 5250 50  0001 C CNN
F 3 "" H 2050 5250 50  0001 C CNN
	1    2050 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR025
U 1 1 61763A2F
P 2350 5000
F 0 "#PWR025" H 2350 4850 50  0001 C CNN
F 1 "+5V" H 2365 5173 50  0000 C CNN
F 2 "" H 2350 5000 50  0001 C CNN
F 3 "" H 2350 5000 50  0001 C CNN
	1    2350 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR029
U 1 1 61764BBF
P 4200 5400
F 0 "#PWR029" H 4200 5250 50  0001 C CNN
F 1 "+12V" H 4215 5573 50  0000 C CNN
F 2 "" H 4200 5400 50  0001 C CNN
F 3 "" H 4200 5400 50  0001 C CNN
	1    4200 5400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR033
U 1 1 61764F99
P 4200 6000
F 0 "#PWR033" H 4200 5850 50  0001 C CNN
F 1 "+12V" H 4215 6173 50  0000 C CNN
F 2 "" H 4200 6000 50  0001 C CNN
F 3 "" H 4200 6000 50  0001 C CNN
	1    4200 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR037
U 1 1 61765D9B
P 4200 6600
F 0 "#PWR037" H 4200 6450 50  0001 C CNN
F 1 "+12V" H 4215 6773 50  0000 C CNN
F 2 "" H 4200 6600 50  0001 C CNN
F 3 "" H 4200 6600 50  0001 C CNN
	1    4200 6600
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR038
U 1 1 6176609E
P 4200 7200
F 0 "#PWR038" H 4200 7050 50  0001 C CNN
F 1 "+12V" H 4215 7373 50  0000 C CNN
F 2 "" H 4200 7200 50  0001 C CNN
F 3 "" H 4200 7200 50  0001 C CNN
	1    4200 7200
	1    0    0    -1  
$EndComp
Text Label 7700 900  0    50   ~ 0
LED0_R
Text Label 4200 5500 2    50   ~ 0
LED0_R
Text Label 4200 5600 2    50   ~ 0
LED0_G
Text Label 4200 5700 2    50   ~ 0
LED0_B
Text Label 4200 6100 2    50   ~ 0
LED1_R
Text Label 4200 6200 2    50   ~ 0
LED1_G
Text Label 4200 6300 2    50   ~ 0
LED1_B
Text Label 4200 6700 2    50   ~ 0
LED2_R
Text Label 4200 6800 2    50   ~ 0
LED2_G
Text Label 4200 6900 2    50   ~ 0
LED2_B
Text Label 4200 7300 2    50   ~ 0
LED3_R
Text Label 4200 7400 2    50   ~ 0
LED3_G
Text Label 4200 7500 2    50   ~ 0
LED3_B
Text Label 7100 1100 2    50   ~ 0
Do_LED0_R
NoConn ~ 2200 1700
NoConn ~ 5700 1450
$Comp
L power:+5V #PWR09
U 1 1 61769AA4
P 2000 1700
F 0 "#PWR09" H 2000 1550 50  0001 C CNN
F 1 "+5V" H 2015 1873 50  0000 C CNN
F 2 "" H 2000 1700 50  0001 C CNN
F 3 "" H 2000 1700 50  0001 C CNN
	1    2000 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR07
U 1 1 6176A8BA
P 6000 1450
F 0 "#PWR07" H 6000 1300 50  0001 C CNN
F 1 "+5V" H 6015 1623 50  0000 C CNN
F 2 "" H 6000 1450 50  0001 C CNN
F 3 "" H 6000 1450 50  0001 C CNN
	1    6000 1450
	1    0    0    -1  
$EndComp
NoConn ~ 5900 1450
Text Label 1700 2400 2    50   ~ 0
Wr_Nt
Text Label 1700 2500 2    50   ~ 0
Wt_Nr
Text Label 5300 1950 2    50   ~ 0
Wr_Nt
Text Label 5300 1850 2    50   ~ 0
Wt_Nr
NoConn ~ 1700 2100
NoConn ~ 6300 1850
NoConn ~ 6300 1950
$Comp
L power:GND #PWR032
U 1 1 6176C005
P 3250 5800
F 0 "#PWR032" H 3250 5550 50  0001 C CNN
F 1 "GND" H 3255 5627 50  0000 C CNN
F 2 "" H 3250 5800 50  0001 C CNN
F 3 "" H 3250 5800 50  0001 C CNN
	1    3250 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR031
U 1 1 6176CB7B
P 3000 5800
F 0 "#PWR031" H 3000 5600 50  0001 C CNN
F 1 "GNDPWR" H 3004 5646 50  0000 C CNN
F 2 "" H 3000 5750 50  0001 C CNN
F 3 "" H 3000 5750 50  0001 C CNN
	1    3000 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT1
U 1 1 6176D6CF
P 3100 5750
F 0 "NT1" H 3100 5931 50  0000 C CNN
F 1 "Net-Tie_2" H 3100 5840 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 3100 5750 50  0001 C CNN
F 3 "~" H 3100 5750 50  0001 C CNN
	1    3100 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 5800 3000 5750
Wire Wire Line
	3200 5750 3250 5750
Wire Wire Line
	3250 5750 3250 5800
$Comp
L power:GND #PWR016
U 1 1 6176E090
P 2100 3300
F 0 "#PWR016" H 2100 3050 50  0001 C CNN
F 1 "GND" H 2105 3127 50  0000 C CNN
F 2 "" H 2100 3300 50  0001 C CNN
F 3 "" H 2100 3300 50  0001 C CNN
	1    2100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 6176EE2C
P 5850 3500
F 0 "#PWR017" H 5850 3250 50  0001 C CNN
F 1 "GND" H 5855 3327 50  0000 C CNN
F 2 "" H 5850 3500 50  0001 C CNN
F 3 "" H 5850 3500 50  0001 C CNN
	1    5850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3450 5900 3500
Wire Wire Line
	5900 3500 5850 3500
Wire Wire Line
	5800 3500 5800 3450
Connection ~ 5850 3500
Wire Wire Line
	5850 3500 5800 3500
Text Label 2500 2200 0    50   ~ 0
Do_LED0_R
Text Label 2500 2300 0    50   ~ 0
Do_LED0_G
Text Label 2500 2400 0    50   ~ 0
Do_LED0_B
Text Label 2500 2600 0    50   ~ 0
Do_LED1_R
Text Label 2500 2700 0    50   ~ 0
Do_LED1_G
Text Label 2500 2800 0    50   ~ 0
Do_LED1_B
Text Label 2800 2900 0    50   ~ 0
Do_NeoPixel
Text Label 5300 2150 2    50   ~ 0
Do_LED2_R
Text Label 5300 2350 2    50   ~ 0
Do_LED2_G
Text Label 5300 2450 2    50   ~ 0
Do_LED2_B
Text Label 5300 2750 2    50   ~ 0
Do_LED3_R
Text Label 5300 2850 2    50   ~ 0
Do_LED3_G
Text Label 5300 2950 2    50   ~ 0
Do_LED3_B
Text Label 2500 2000 0    50   ~ 0
Ai_MIC
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 61776360
P 9100 1100
F 0 "Q2" H 9305 1146 50  0000 L CNN
F 1 "AP4438" H 9305 1055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 9300 1200 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 9100 1100 50  0001 C CNN
F 4 "C353072" H 9100 1100 50  0001 C CNN "LCSC"
	1    9100 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6177636A
P 8750 1100
F 0 "R2" V 8650 1100 50  0000 C CNN
F 1 "1k" V 8750 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8680 1100 50  0001 C CNN
F 3 "~" H 8750 1100 50  0001 C CNN
	1    8750 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 61776374
P 8900 1250
F 0 "R5" V 8800 1250 50  0000 C CNN
F 1 "470k" V 8900 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 1250 50  0001 C CNN
F 3 "~" H 8900 1250 50  0001 C CNN
	1    8900 1250
	-1   0    0    1   
$EndComp
Connection ~ 8900 1100
Wire Wire Line
	9200 1300 9200 1400
Wire Wire Line
	9200 1400 9100 1400
$Comp
L power:GNDPWR #PWR04
U 1 1 61776381
P 9100 1400
F 0 "#PWR04" H 9100 1200 50  0001 C CNN
F 1 "GNDPWR" H 9104 1246 50  0000 C CNN
F 2 "" H 9100 1350 50  0001 C CNN
F 3 "" H 9100 1350 50  0001 C CNN
	1    9100 1400
	1    0    0    -1  
$EndComp
Connection ~ 9100 1400
Wire Wire Line
	9100 1400 8900 1400
Text Label 9200 900  0    50   ~ 0
LED0_G
Text Label 8600 1100 2    50   ~ 0
Do_LED0_G
$Comp
L Device:Q_NMOS_GDS Q3
U 1 1 6177862D
P 10600 1100
F 0 "Q3" H 10805 1146 50  0000 L CNN
F 1 "AP4438" H 10805 1055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 10800 1200 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 10600 1100 50  0001 C CNN
F 4 "C353072" H 10600 1100 50  0001 C CNN "LCSC"
	1    10600 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 6177865F
P 10250 1100
F 0 "R3" V 10150 1100 50  0000 C CNN
F 1 "1k" V 10250 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10180 1100 50  0001 C CNN
F 3 "~" H 10250 1100 50  0001 C CNN
	1    10250 1100
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 61778669
P 10400 1250
F 0 "R6" V 10300 1250 50  0000 C CNN
F 1 "470k" V 10400 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10330 1250 50  0001 C CNN
F 3 "~" H 10400 1250 50  0001 C CNN
	1    10400 1250
	-1   0    0    1   
$EndComp
Connection ~ 10400 1100
Wire Wire Line
	10700 1300 10700 1400
Wire Wire Line
	10700 1400 10600 1400
$Comp
L power:GNDPWR #PWR05
U 1 1 61778676
P 10600 1400
F 0 "#PWR05" H 10600 1200 50  0001 C CNN
F 1 "GNDPWR" H 10604 1246 50  0000 C CNN
F 2 "" H 10600 1350 50  0001 C CNN
F 3 "" H 10600 1350 50  0001 C CNN
	1    10600 1400
	1    0    0    -1  
$EndComp
Connection ~ 10600 1400
Wire Wire Line
	10600 1400 10400 1400
Text Label 10700 900  0    50   ~ 0
LED0_B
Text Label 10100 1100 2    50   ~ 0
Do_LED0_B
$Comp
L Device:Q_NMOS_GDS Q4
U 1 1 6177A560
P 7600 2000
F 0 "Q4" H 7805 2046 50  0000 L CNN
F 1 "AP4438" H 7805 1955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 7800 2100 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 7600 2000 50  0001 C CNN
F 4 "C353072" H 7600 2000 50  0001 C CNN "LCSC"
	1    7600 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 6177A592
P 7250 2000
F 0 "R7" V 7150 2000 50  0000 C CNN
F 1 "1k" V 7250 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 2000 50  0001 C CNN
F 3 "~" H 7250 2000 50  0001 C CNN
	1    7250 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 6177A59C
P 7400 2150
F 0 "R10" V 7300 2150 50  0000 C CNN
F 1 "470k" V 7400 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7330 2150 50  0001 C CNN
F 3 "~" H 7400 2150 50  0001 C CNN
	1    7400 2150
	-1   0    0    1   
$EndComp
Connection ~ 7400 2000
Wire Wire Line
	7700 2200 7700 2300
Wire Wire Line
	7700 2300 7600 2300
$Comp
L power:GNDPWR #PWR010
U 1 1 6177A5A9
P 7600 2300
F 0 "#PWR010" H 7600 2100 50  0001 C CNN
F 1 "GNDPWR" H 7604 2146 50  0000 C CNN
F 2 "" H 7600 2250 50  0001 C CNN
F 3 "" H 7600 2250 50  0001 C CNN
	1    7600 2300
	1    0    0    -1  
$EndComp
Connection ~ 7600 2300
Wire Wire Line
	7600 2300 7400 2300
Text Label 7700 1800 0    50   ~ 0
LED1_R
Text Label 7100 2000 2    50   ~ 0
Do_LED1_R
$Comp
L Device:Q_NMOS_GDS Q5
U 1 1 6177A5B8
P 9100 2000
F 0 "Q5" H 9305 2046 50  0000 L CNN
F 1 "AP4438" H 9305 1955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 9300 2100 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 9100 2000 50  0001 C CNN
F 4 "C353072" H 9100 2000 50  0001 C CNN "LCSC"
	1    9100 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 6177A5C2
P 8750 2000
F 0 "R8" V 8650 2000 50  0000 C CNN
F 1 "1k" V 8750 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8680 2000 50  0001 C CNN
F 3 "~" H 8750 2000 50  0001 C CNN
	1    8750 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 6177A5CC
P 8900 2150
F 0 "R11" V 8800 2150 50  0000 C CNN
F 1 "470k" V 8900 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 2150 50  0001 C CNN
F 3 "~" H 8900 2150 50  0001 C CNN
	1    8900 2150
	-1   0    0    1   
$EndComp
Connection ~ 8900 2000
Wire Wire Line
	9200 2200 9200 2300
Wire Wire Line
	9200 2300 9100 2300
$Comp
L power:GNDPWR #PWR011
U 1 1 6177A5D9
P 9100 2300
F 0 "#PWR011" H 9100 2100 50  0001 C CNN
F 1 "GNDPWR" H 9104 2146 50  0000 C CNN
F 2 "" H 9100 2250 50  0001 C CNN
F 3 "" H 9100 2250 50  0001 C CNN
	1    9100 2300
	1    0    0    -1  
$EndComp
Connection ~ 9100 2300
Wire Wire Line
	9100 2300 8900 2300
Text Label 9200 1800 0    50   ~ 0
LED1_G
Text Label 8600 2000 2    50   ~ 0
Do_LED1_G
$Comp
L Device:Q_NMOS_GDS Q6
U 1 1 6177A5E8
P 10600 2000
F 0 "Q6" H 10805 2046 50  0000 L CNN
F 1 "AP4438" H 10805 1955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 10800 2100 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 10600 2000 50  0001 C CNN
F 4 "C353072" H 10600 2000 50  0001 C CNN "LCSC"
	1    10600 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 6177A5F2
P 10250 2000
F 0 "R9" V 10150 2000 50  0000 C CNN
F 1 "1k" V 10250 2000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10180 2000 50  0001 C CNN
F 3 "~" H 10250 2000 50  0001 C CNN
	1    10250 2000
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 6177A5FC
P 10400 2150
F 0 "R12" V 10300 2150 50  0000 C CNN
F 1 "470k" V 10400 2150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10330 2150 50  0001 C CNN
F 3 "~" H 10400 2150 50  0001 C CNN
	1    10400 2150
	-1   0    0    1   
$EndComp
Connection ~ 10400 2000
Wire Wire Line
	10700 2200 10700 2300
Wire Wire Line
	10700 2300 10600 2300
$Comp
L power:GNDPWR #PWR012
U 1 1 6177A609
P 10600 2300
F 0 "#PWR012" H 10600 2100 50  0001 C CNN
F 1 "GNDPWR" H 10604 2146 50  0000 C CNN
F 2 "" H 10600 2250 50  0001 C CNN
F 3 "" H 10600 2250 50  0001 C CNN
	1    10600 2300
	1    0    0    -1  
$EndComp
Connection ~ 10600 2300
Wire Wire Line
	10600 2300 10400 2300
Text Label 10700 1800 0    50   ~ 0
LED1_B
Text Label 10100 2000 2    50   ~ 0
Do_LED1_B
$Comp
L Device:R R14
U 1 1 6177D0DF
P 7250 2900
F 0 "R14" V 7150 2900 50  0000 C CNN
F 1 "1k" V 7250 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 2900 50  0001 C CNN
F 3 "~" H 7250 2900 50  0001 C CNN
	1    7250 2900
	0    1    1    0   
$EndComp
Connection ~ 7400 2900
Wire Wire Line
	7700 3200 7600 3200
$Comp
L power:GNDPWR #PWR013
U 1 1 6177D12A
P 7600 3200
F 0 "#PWR013" H 7600 3000 50  0001 C CNN
F 1 "GNDPWR" H 7604 3046 50  0000 C CNN
F 2 "" H 7600 3150 50  0001 C CNN
F 3 "" H 7600 3150 50  0001 C CNN
	1    7600 3200
	1    0    0    -1  
$EndComp
Connection ~ 7600 3200
Wire Wire Line
	7600 3200 7400 3200
Text Label 7700 2700 0    50   ~ 0
LED2_R
Text Label 7100 2900 2    50   ~ 0
Do_LED2_R
$Comp
L Device:Q_NMOS_GDS Q8
U 1 1 6177D139
P 9100 2900
F 0 "Q8" H 9305 2946 50  0000 L CNN
F 1 "AP4438" H 9305 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 9300 3000 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 9100 2900 50  0001 C CNN
F 4 "C353072" H 9100 2900 50  0001 C CNN "LCSC"
	1    9100 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 6177D143
P 8750 2900
F 0 "R15" V 8650 2900 50  0000 C CNN
F 1 "1k" V 8750 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8680 2900 50  0001 C CNN
F 3 "~" H 8750 2900 50  0001 C CNN
	1    8750 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 6177D14D
P 8900 3050
F 0 "R18" V 8800 3050 50  0000 C CNN
F 1 "470k" V 8900 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 3050 50  0001 C CNN
F 3 "~" H 8900 3050 50  0001 C CNN
	1    8900 3050
	-1   0    0    1   
$EndComp
Connection ~ 8900 2900
Wire Wire Line
	9200 3100 9200 3200
Wire Wire Line
	9200 3200 9100 3200
$Comp
L power:GNDPWR #PWR014
U 1 1 6177D15A
P 9100 3200
F 0 "#PWR014" H 9100 3000 50  0001 C CNN
F 1 "GNDPWR" H 9104 3046 50  0000 C CNN
F 2 "" H 9100 3150 50  0001 C CNN
F 3 "" H 9100 3150 50  0001 C CNN
	1    9100 3200
	1    0    0    -1  
$EndComp
Connection ~ 9100 3200
Wire Wire Line
	9100 3200 8900 3200
Text Label 9200 2700 0    50   ~ 0
LED2_G
Text Label 8600 2900 2    50   ~ 0
Do_LED2_G
$Comp
L Device:Q_NMOS_GDS Q9
U 1 1 6177D169
P 10600 2900
F 0 "Q9" H 10805 2946 50  0000 L CNN
F 1 "AP4438" H 10805 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 10800 3000 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 10600 2900 50  0001 C CNN
F 4 "C353072" H 10600 2900 50  0001 C CNN "LCSC"
	1    10600 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 6177D173
P 10250 2900
F 0 "R16" V 10150 2900 50  0000 C CNN
F 1 "1k" V 10250 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10180 2900 50  0001 C CNN
F 3 "~" H 10250 2900 50  0001 C CNN
	1    10250 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R19
U 1 1 6177D17D
P 10400 3050
F 0 "R19" V 10300 3050 50  0000 C CNN
F 1 "470k" V 10400 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10330 3050 50  0001 C CNN
F 3 "~" H 10400 3050 50  0001 C CNN
	1    10400 3050
	-1   0    0    1   
$EndComp
Connection ~ 10400 2900
Wire Wire Line
	10700 3100 10700 3200
Wire Wire Line
	10700 3200 10600 3200
$Comp
L power:GNDPWR #PWR015
U 1 1 6177D18A
P 10600 3200
F 0 "#PWR015" H 10600 3000 50  0001 C CNN
F 1 "GNDPWR" H 10604 3046 50  0000 C CNN
F 2 "" H 10600 3150 50  0001 C CNN
F 3 "" H 10600 3150 50  0001 C CNN
	1    10600 3200
	1    0    0    -1  
$EndComp
Connection ~ 10600 3200
Wire Wire Line
	10600 3200 10400 3200
Text Label 10700 2700 0    50   ~ 0
LED2_B
Text Label 10100 2900 2    50   ~ 0
Do_LED2_B
$Comp
L Device:Q_NMOS_GDS Q10
U 1 1 6177FFF4
P 7600 3750
F 0 "Q10" H 7805 3796 50  0000 L CNN
F 1 "AP4438" H 7805 3705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 7800 3850 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 7600 3750 50  0001 C CNN
F 4 "C353072" H 7600 3750 50  0001 C CNN "LCSC"
	1    7600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 61780026
P 7250 3750
F 0 "R20" V 7150 3750 50  0000 C CNN
F 1 "1k" V 7250 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7180 3750 50  0001 C CNN
F 3 "~" H 7250 3750 50  0001 C CNN
	1    7250 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R23
U 1 1 61780030
P 7400 3900
F 0 "R23" V 7300 3900 50  0000 C CNN
F 1 "470k" V 7400 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7330 3900 50  0001 C CNN
F 3 "~" H 7400 3900 50  0001 C CNN
	1    7400 3900
	-1   0    0    1   
$EndComp
Connection ~ 7400 3750
Wire Wire Line
	7700 3950 7700 4050
Wire Wire Line
	7700 4050 7600 4050
$Comp
L power:GNDPWR #PWR018
U 1 1 6178003D
P 7600 4050
F 0 "#PWR018" H 7600 3850 50  0001 C CNN
F 1 "GNDPWR" H 7604 3896 50  0000 C CNN
F 2 "" H 7600 4000 50  0001 C CNN
F 3 "" H 7600 4000 50  0001 C CNN
	1    7600 4050
	1    0    0    -1  
$EndComp
Connection ~ 7600 4050
Wire Wire Line
	7600 4050 7400 4050
Text Label 7700 3550 0    50   ~ 0
LED3_R
Text Label 7100 3750 2    50   ~ 0
Do_LED3_R
$Comp
L Device:Q_NMOS_GDS Q11
U 1 1 6178004C
P 9100 3750
F 0 "Q11" H 9305 3796 50  0000 L CNN
F 1 "AP4438" H 9305 3705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 9300 3850 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 9100 3750 50  0001 C CNN
F 4 "C353072" H 9100 3750 50  0001 C CNN "LCSC"
	1    9100 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 61780056
P 8750 3750
F 0 "R21" V 8650 3750 50  0000 C CNN
F 1 "1k" V 8750 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8680 3750 50  0001 C CNN
F 3 "~" H 8750 3750 50  0001 C CNN
	1    8750 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R24
U 1 1 61780060
P 8900 3900
F 0 "R24" V 8800 3900 50  0000 C CNN
F 1 "470k" V 8900 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8830 3900 50  0001 C CNN
F 3 "~" H 8900 3900 50  0001 C CNN
	1    8900 3900
	-1   0    0    1   
$EndComp
Connection ~ 8900 3750
Wire Wire Line
	9200 3950 9200 4050
Wire Wire Line
	9200 4050 9100 4050
$Comp
L power:GNDPWR #PWR019
U 1 1 6178006D
P 9100 4050
F 0 "#PWR019" H 9100 3850 50  0001 C CNN
F 1 "GNDPWR" H 9104 3896 50  0000 C CNN
F 2 "" H 9100 4000 50  0001 C CNN
F 3 "" H 9100 4000 50  0001 C CNN
	1    9100 4050
	1    0    0    -1  
$EndComp
Connection ~ 9100 4050
Wire Wire Line
	9100 4050 8900 4050
Text Label 9200 3550 0    50   ~ 0
LED3_G
Text Label 8600 3750 2    50   ~ 0
Do_LED3_G
$Comp
L Device:Q_NMOS_GDS Q12
U 1 1 6178007C
P 10600 3750
F 0 "Q12" H 10805 3796 50  0000 L CNN
F 1 "AP4438" H 10805 3705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 10800 3850 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 10600 3750 50  0001 C CNN
F 4 "C353072" H 10600 3750 50  0001 C CNN "LCSC"
	1    10600 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 61780086
P 10250 3750
F 0 "R22" V 10150 3750 50  0000 C CNN
F 1 "1k" V 10250 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10180 3750 50  0001 C CNN
F 3 "~" H 10250 3750 50  0001 C CNN
	1    10250 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 61780090
P 10400 3900
F 0 "R25" V 10300 3900 50  0000 C CNN
F 1 "470k" V 10400 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10330 3900 50  0001 C CNN
F 3 "~" H 10400 3900 50  0001 C CNN
	1    10400 3900
	-1   0    0    1   
$EndComp
Connection ~ 10400 3750
Wire Wire Line
	10700 3950 10700 4050
Wire Wire Line
	10700 4050 10600 4050
$Comp
L power:GNDPWR #PWR020
U 1 1 6178009D
P 10600 4050
F 0 "#PWR020" H 10600 3850 50  0001 C CNN
F 1 "GNDPWR" H 10604 3896 50  0000 C CNN
F 2 "" H 10600 4000 50  0001 C CNN
F 3 "" H 10600 4000 50  0001 C CNN
	1    10600 4050
	1    0    0    -1  
$EndComp
Connection ~ 10600 4050
Wire Wire Line
	10600 4050 10400 4050
Text Label 10700 3550 0    50   ~ 0
LED3_B
Text Label 10100 3750 2    50   ~ 0
Do_LED3_B
$Comp
L Device:R R13
U 1 1 61783E1E
P 2650 2900
F 0 "R13" V 2750 2900 50  0000 C CNN
F 1 "470" V 2650 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2580 2900 50  0001 C CNN
F 3 "~" H 2650 2900 50  0001 C CNN
	1    2650 2900
	0    1    1    0   
$EndComp
Text Label 2500 2100 0    50   ~ 0
Di_IR
$Comp
L Interface_Optical:TSOP45xx U3
U 1 1 61787EB1
P 7900 5450
F 0 "U3" H 7888 5875 50  0000 C CNN
F 1 "TSOP45xx" H 7888 5784 50  0000 C CNN
F 2 "OptoDevice:Vishay_MOLD-3Pin" H 7850 5075 50  0001 C CNN
F 3 "http://www.vishay.com/docs/82460/tsop45.pdf" H 8550 5750 50  0001 C CNN
	1    7900 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR027
U 1 1 61789350
P 8300 5250
F 0 "#PWR027" H 8300 5100 50  0001 C CNN
F 1 "+5V" H 8315 5423 50  0000 C CNN
F 2 "" H 8300 5250 50  0001 C CNN
F 3 "" H 8300 5250 50  0001 C CNN
	1    8300 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 61789BAB
P 8300 5650
F 0 "#PWR030" H 8300 5400 50  0001 C CNN
F 1 "GND" H 8305 5477 50  0000 C CNN
F 2 "" H 8300 5650 50  0001 C CNN
F 3 "" H 8300 5650 50  0001 C CNN
	1    8300 5650
	1    0    0    -1  
$EndComp
Text Label 8300 5450 0    50   ~ 0
Di_IR
$Comp
L Device:CP C3
U 1 1 61773F95
P 1050 5100
F 0 "C3" H 1168 5146 50  0000 L CNN
F 1 "47u" H 1168 5055 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 1088 4950 50  0001 C CNN
F 3 "https://www.mouser.fr/ProductDetail/710-860020472006" H 1050 5100 50  0001 C CNN
	1    1050 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 61774F27
P 2600 1450
F 0 "C2" H 2715 1496 50  0000 L CNN
F 1 "1u" H 2715 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2638 1300 50  0001 C CNN
F 3 "~" H 2600 1450 50  0001 C CNN
	1    2600 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 617757C8
P 2600 1300
F 0 "#PWR02" H 2600 1150 50  0001 C CNN
F 1 "+5V" H 2615 1473 50  0000 C CNN
F 2 "" H 2600 1300 50  0001 C CNN
F 3 "" H 2600 1300 50  0001 C CNN
	1    2600 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 61775BF0
P 2600 1600
F 0 "#PWR08" H 2600 1350 50  0001 C CNN
F 1 "GND" H 2605 1427 50  0000 C CNN
F 2 "" H 2600 1600 50  0001 C CNN
F 3 "" H 2600 1600 50  0001 C CNN
	1    2600 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6177627C
P 5150 1300
F 0 "C1" H 5265 1346 50  0000 L CNN
F 1 "1u" H 5265 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5188 1150 50  0001 C CNN
F 3 "~" H 5150 1300 50  0001 C CNN
	1    5150 1300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 61776286
P 5150 1150
F 0 "#PWR01" H 5150 1000 50  0001 C CNN
F 1 "+5V" H 5165 1323 50  0000 C CNN
F 2 "" H 5150 1150 50  0001 C CNN
F 3 "" H 5150 1150 50  0001 C CNN
	1    5150 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 61776290
P 5150 1450
F 0 "#PWR06" H 5150 1200 50  0001 C CNN
F 1 "GND" H 5155 1277 50  0000 C CNN
F 2 "" H 5150 1450 50  0001 C CNN
F 3 "" H 5150 1450 50  0001 C CNN
	1    5150 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR023
U 1 1 61779690
P 1050 4950
F 0 "#PWR023" H 1050 4800 50  0001 C CNN
F 1 "+12V" H 1065 5123 50  0000 C CNN
F 2 "" H 1050 4950 50  0001 C CNN
F 3 "" H 1050 4950 50  0001 C CNN
	1    1050 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR026
U 1 1 61779B05
P 1050 5250
F 0 "#PWR026" H 1050 5050 50  0001 C CNN
F 1 "GNDPWR" H 1054 5096 50  0000 C CNN
F 2 "" H 1050 5200 50  0001 C CNN
F 3 "" H 1050 5200 50  0001 C CNN
	1    1050 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 6177B227
P 1550 5150
F 0 "C4" H 1665 5196 50  0000 L CNN
F 1 "10u" H 1665 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 1588 5000 50  0001 C CNN
F 3 "https://www.mouser.fr/ProductDetail/TDK/C3216X5R1C106M160AA" H 1550 5150 50  0001 C CNN
F 4 "16V" H 1550 5150 50  0001 C CNN "Umax"
	1    1550 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 6177B948
P 2550 5150
F 0 "C5" H 2665 5196 50  0000 L CNN
F 1 "10u" H 2665 5105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 2588 5000 50  0001 C CNN
F 3 "https://www.mouser.fr/ProductDetail/TDK/C3216X5R1C106M160AA" H 2550 5150 50  0001 C CNN
F 4 "16V" H 2550 5150 50  0001 C CNN "Umax"
	1    2550 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5000 2350 5000
Connection ~ 2350 5000
Wire Wire Line
	1550 5000 1750 5000
Connection ~ 1750 5000
Wire Wire Line
	1550 5300 2050 5300
Connection ~ 2050 5300
Wire Wire Line
	2050 5300 2550 5300
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61782A8D
P 5900 6000
F 0 "#FLG01" H 5900 6075 50  0001 C CNN
F 1 "PWR_FLAG" H 5900 6173 50  0000 C CNN
F 2 "" H 5900 6000 50  0001 C CNN
F 3 "~" H 5900 6000 50  0001 C CNN
	1    5900 6000
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR034
U 1 1 61782BD4
P 5900 6000
F 0 "#PWR034" H 5900 5850 50  0001 C CNN
F 1 "+12V" H 5915 6173 50  0000 C CNN
F 2 "" H 5900 6000 50  0001 C CNN
F 3 "" H 5900 6000 50  0001 C CNN
	1    5900 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR035
U 1 1 6178382B
P 6350 6000
F 0 "#PWR035" H 6350 5800 50  0001 C CNN
F 1 "GNDPWR" H 6354 5846 50  0000 C CNN
F 2 "" H 6350 5950 50  0001 C CNN
F 3 "" H 6350 5950 50  0001 C CNN
	1    6350 6000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 61783E4D
P 6350 6000
F 0 "#FLG02" H 6350 6075 50  0001 C CNN
F 1 "PWR_FLAG" H 6350 6173 50  0000 C CNN
F 2 "" H 6350 6000 50  0001 C CNN
F 3 "~" H 6350 6000 50  0001 C CNN
	1    6350 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR036
U 1 1 61784314
P 6750 6000
F 0 "#PWR036" H 6750 5750 50  0001 C CNN
F 1 "GND" H 6755 5827 50  0000 C CNN
F 2 "" H 6750 6000 50  0001 C CNN
F 3 "" H 6750 6000 50  0001 C CNN
	1    6750 6000
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 617846C8
P 6750 6000
F 0 "#FLG03" H 6750 6075 50  0001 C CNN
F 1 "PWR_FLAG" H 6750 6173 50  0000 C CNN
F 2 "" H 6750 6000 50  0001 C CNN
F 3 "~" H 6750 6000 50  0001 C CNN
	1    6750 6000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 6177D11D
P 7400 3050
F 0 "R17" V 7300 3050 50  0000 C CNN
F 1 "470k" V 7400 3050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7330 3050 50  0001 C CNN
F 3 "~" H 7400 3050 50  0001 C CNN
	1    7400 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	7700 3100 7700 3200
$Comp
L Device:Q_NMOS_GDS Q7
U 1 1 6177D0D5
P 7600 2900
F 0 "Q7" H 7805 2946 50  0000 L CNN
F 1 "AP4438" H 7805 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-89-3" H 7800 3000 50  0001 C CNN
F 3 "https://www.lcsc.com/product-detail/MOSFETs_ALLPOWER-ShenZhen-Quan-Li-Semiconductor-AP4438_C353072.html" H 7600 2900 50  0001 C CNN
F 4 "C353072" H 7600 2900 50  0001 C CNN "LCSC"
	1    7600 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J8
U 1 1 6184504C
P 2200 7150
F 0 "J8" H 2228 7171 50  0000 L CNN
F 1 "NeoPixel" H 2228 7080 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2200 7150 50  0001 C CNN
F 3 "~" H 2200 7150 50  0001 C CNN
F 4 "NeoPixel" H 2228 6989 50  0000 L CNN "Info"
	1    2200 7150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 6184702D
P 2000 7050
F 0 "#PWR0101" H 2000 6900 50  0001 C CNN
F 1 "+5V" H 2015 7223 50  0000 C CNN
F 2 "" H 2000 7050 50  0001 C CNN
F 3 "" H 2000 7050 50  0001 C CNN
	1    2000 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 61849468
P 2000 7250
F 0 "#PWR0102" H 2000 7050 50  0001 C CNN
F 1 "GNDPWR" H 2004 7096 50  0000 C CNN
F 2 "" H 2000 7200 50  0001 C CNN
F 3 "" H 2000 7200 50  0001 C CNN
	1    2000 7250
	1    0    0    -1  
$EndComp
Text Label 2000 7150 2    50   ~ 0
Do_NeoPixel
$Comp
L Connector:Conn_01x03_Female J7
U 1 1 6184BD33
P 2200 6500
F 0 "J7" H 2228 6521 50  0000 L CNN
F 1 "Mic" H 2228 6430 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2200 6500 50  0001 C CNN
F 3 "~" H 2200 6500 50  0001 C CNN
F 4 "Mic" H 2228 6339 50  0000 L CNN "Info"
	1    2200 6500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 6184BD55
P 2000 6400
F 0 "#PWR0103" H 2000 6250 50  0001 C CNN
F 1 "+5V" H 2015 6573 50  0000 C CNN
F 2 "" H 2000 6400 50  0001 C CNN
F 3 "" H 2000 6400 50  0001 C CNN
	1    2000 6400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6184D686
P 2000 6600
F 0 "#PWR0104" H 2000 6350 50  0001 C CNN
F 1 "GND" H 2005 6427 50  0000 C CNN
F 2 "" H 2000 6600 50  0001 C CNN
F 3 "" H 2000 6600 50  0001 C CNN
	1    2000 6600
	1    0    0    -1  
$EndComp
Text Label 2000 6500 2    50   ~ 0
Ai_MIC
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO1
U 1 1 6196C55E
P 9650 5750
F 0 "LOGO1" H 9650 6250 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 9650 5350 50  0001 C CNN
F 2 "GoInK:GoInK300" H 9650 5750 50  0001 C CNN
F 3 "~" H 9650 5750 50  0001 C CNN
	1    9650 5750
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small LOGO2
U 1 1 61970C6D
P 10200 5750
F 0 "LOGO2" H 10200 6250 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 10200 5350 50  0001 C CNN
F 2 "GoInK:GoInK300" H 10200 5750 50  0001 C CNN
F 3 "~" H 10200 5750 50  0001 C CNN
	1    10200 5750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
