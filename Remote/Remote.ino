//Original oled from :
// https://github.com/G6EJD/Wemos-1.4TFT-Shield-Examples/blob/master/ESP8266_14_TFT_Test.ino
//Screen SSD1306 : 128×64 : connected to D1 (SDA) + D2 (SCL)

#include "SSD1306.h"
#include <ESP8266WiFi.h>
//#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include "credentials.h"

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64

#define LED	D0
#define BTN_ENT		D5
#define BTN_UP		D6
#define BTN_DOWN	D7

//create display(Adr, SDA-pin, SCL-pin)
//SSD1306 display(0x3c, 5, 4); //GPIO 5 = D1, GPIO 4 = D2
SSD1306	display(0x3c, 5 /*D1*/, 4 /*D2*/);

WiFiClient client;

const char* plafondIP = "192.168.1.240";

IPAddress ip(192, 168, 1, 241); // where xx is the desired IP Address
IPAddress gateway(192, 168, 1, 254); // set gateway to match your network

//HTTPClient httpRed;  //Declare an object of class HTTPClient
//HTTPClient httpOff;  //Declare an object of class HTTPClient

void setup() {

	Serial.begin(115200);
	
	pinMode(LED, OUTPUT);
	pinMode(BTN_ENT, INPUT_PULLUP);
	pinMode(BTN_UP, INPUT_PULLUP);
	pinMode(BTN_DOWN, INPUT_PULLUP);

	display.init();
	display.flipScreenVertically();
	display.setFont(ArialMT_Plain_16);
	display.setTextAlignment(TEXT_ALIGN_CENTER);
	display.drawString(64, 0, "Remote control");
	display.setFont(ArialMT_Plain_10);
	display.drawString(64, 16, "IP=192.168.1.241");
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.drawString(0, 26, "To : "+String(ssid));
	display.display();
	
	// Attempt to connect to Wifi network:
	Serial.print("Connecting Wifi: ");
	Serial.println(ssid);
	
	IPAddress subnet(255, 255, 255, 0); // set subnet mask to match your
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(ssid, password);
	
	byte cntDot=0;
	int i;
	while (WiFi.status() != WL_CONNECTED) {
		display.drawProgressBar(0, 40, 126, 10, cntDot);
		cntDot++;
		display.display();
		Serial.print(".");
		if (cntDot%2) {
			for (i = 0; i < 255; i += 1) {
				analogWrite(LED, i);
				delay(2);
			}
		} else {
			for (i = 255; i > 0; i -= 1) {
				analogWrite(LED, i);
				delay(2);
			}
		}
	}
	if (!client.connect(plafondIP, 80)) {
		Serial.println("connection failed");
		display.drawString(0, 54, "Fail connect");
		delay(5000);
	} else {
		display.drawString(0, 54, "Connection to plafond ok");
		Serial.println("Connection to plafond ok");
		/*
		client.print(String("GET /slider?position=1&r=0&g=50&b=0") + " HTTP/1.1\r\n" +
		"Host: " + plafondIP + "\r\n" +
		"Connection: close\r\n\r\n");
		
		delay(500);
		
		httpRed.begin(client, "http://192.168.1.240/slider?position=1&r=50&g=0&b=0");
		httpOff.begin(client, "http://192.168.1.240/slider?position=1&r=0&g=0&b=0");
		*/
	}
	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	IPAddress ip = WiFi.localIP();
	Serial.println(ip);
	DrawMenu(0);
}


void loop() {
	byte line=0;
	static byte oldLine=0;
	byte buttonClicked=0;		//1=Down, 2=Enter, 3=Up
	
	digitalWrite(LED, HIGH);
	
	if (digitalRead(BTN_ENT)==LOW) {
		display.drawString(0, 50, "D5");
		digitalWrite(LED, LOW);
		buttonClicked = 2;
	} else if (digitalRead(BTN_UP)==LOW) {
		display.drawString(20, 50, "D6");
		digitalWrite(LED, LOW);
		buttonClicked = 3;
		SendRequest("slider?position=1&r=50&g=0&b=0");
	} else if (digitalRead(BTN_DOWN)==LOW) {
		display.drawString(40, 50, "D7");
		digitalWrite(LED, LOW);
		SendRequest("slider?position=1&r=0&g=0&b=0");
		buttonClicked = 1;
	}
	display.display();
	
	while (digitalRead(BTN_DOWN)==LOW || digitalRead(BTN_ENT)==LOW || digitalRead(BTN_UP)==LOW) {
		delay(10);
	}
	
	if (buttonClicked!=0) {
		DrawMenu(0);
	}
	delay(10);
}

void DrawMenu(byte line){
	display.clear();
	display.setFont(ArialMT_Plain_10);
	display.setTextAlignment(TEXT_ALIGN_LEFT);
	display.drawString(0, 0, "IP=192.168.1.241");
	display.display();
	if (line==0) {
		
	}
}

void SendRequest(String data){
	Serial.println("Start SendRequest");

		client.connect(plafondIP, 80);
		Serial.println(data);
		client.print(String("GET /") + data + " HTTP/1.1\r\n" +
		"Host: " + plafondIP + "\r\n" +
		"Connection: close\r\n\r\n");
/*
	int httpCode;
	if (data==1) {
		httpCode = httpRed.GET();                                  //Send the request
	} else {
		httpCode = httpOff.GET();
	}
	Serial.println(httpCode);
	if (httpCode > 0) { //Check the returning code
		String payload;
		if (data==1) {
			payload = httpRed.getString();   //Get the request response payload
		} else {
			payload = httpOff.getString();   //Get the request response payload
		}
		Serial.println(payload);             //Print the response payload
	}
	Serial.println("end");
	/*
	if (data==1) {
		httpRed.end();   //Close connection
	} else {
		httpOff.end();   //Close connection
	}
	*/
	Serial.println("Done!");
}
