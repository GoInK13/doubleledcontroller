/*
  Communicate with UART
*/

//Variable globale
#define Do_LED2_R	3
#define Do_LED2_G	5
#define Do_LED2_B	6
#define Do_LED3_R	9
#define Do_LED3_G	10
#define Do_LED3_B	11

#define COLOR_R		0
#define COLOR_G		1
#define COLOR_B		2

#define DBG_UART	0

const int BUFFER_SIZE = 20;
char messageReceived[BUFFER_SIZE];

const int correctPWM[256]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,19,21,23,25,27,30,32,35,37,40,43,46,49,52,55,59,62,66,69,73,77,81,85,89,94,98,103,107,112,117,122,127,133,138,144,149,155,161,167,173,179,185,192,198,205,212,219,226,233,240,248,255,263,271,279,287,295,303,312,320,329,338,347,356,365,374,383,393,403,413,422,433,443,453,464,474,485,496,507,518,529,540,552,564,575,587,599,611,624,636,649,661,674,687,700,714,727,741,754,768,782,796,810,825,839,854,868,883,898,913,929,944,960,975,991,1007,1023,1040,1056,1073,1089,1106,1123,1140,1158,1175,1193,1210,1228,1246,1264,1282,1301,1319,1338,1357,1376,1395,1414,1434,1453,1473,1493,1513,1533,1553,1573,1594,1615,1636,1657,1678,1699,1720,1742,1764,1786,1808,1830,1852,1875,1897,1920,1943,1966,1989,2013,2036,2060,2084,2108,2132,2156,2180,2205,2230,2254,2279,2305,2330,2355,2381,2407,2433,2459,2485,2511,2538,2564,2591,2618,2645,2673,2700,2728,2755,2783,2811,2839,2868,2896,2925,2954,2983,3012,3041,3071,3100,3130,3160,3190,3220,3250,3281,3312,3342,3373,3404,3436,3467,3499,3531,3562,3595,3627,3659,3692,3724,3757,3790,3823,3857,3890,3924,3958,3992,4026,4060,4095};

byte color[2][3];		//position, colors RGB
byte brightness[2]={255,255}, oldBrightness[2];	//255:Max, 0:Off

// Setup lancé une fois au démarrage
void setup() {                
  //Initialisation des broches : In/Out
	Serial.begin(115200);
	pinMode(Do_LED2_R, OUTPUT);
	pinMode(Do_LED2_G, OUTPUT);
	pinMode(Do_LED2_B, OUTPUT);
	pinMode(Do_LED3_R, OUTPUT);
	pinMode(Do_LED3_G, OUTPUT);
	pinMode(Do_LED3_B, OUTPUT);
}

//Boucle de programme
void loop() {
	if (Serial.available() > 0) {
		// read the incoming bytes:
		int rlen = Serial.readBytesUntil('\n', messageReceived, BUFFER_SIZE);
		// prints the received data
#if DBG_UART==1
		Serial.print("I received: ");
		for(int i = 0; i < rlen; i++)
			Serial.print(messageReceived[i]);
#endif
		if (rlen<=7 && messageReceived[0]=='D' && (messageReceived[1]=='0' || messageReceived[1]=='2')) {
			byte LEDNumber = (messageReceived[1]-48)/2;
			byte LEDColor=0;
			if (messageReceived[2]=='R') {
				LEDColor=COLOR_R;
			} else if (messageReceived[2]=='G') {
				LEDColor=COLOR_G;
			} else if (messageReceived[2]=='B') {
				LEDColor=COLOR_B;
			}
			char value[4]={0,0,0,0};
			memcpy (value, messageReceived+3, rlen-3 );
			byte LEDValue = atoi(value);
			color[LEDNumber][LEDColor]=LEDValue;
#if DBG_UART==1
			Serial.print(",Num=");
			Serial.print(LEDNumber);
			Serial.print(",Color=");
			Serial.print(LEDColor);
			Serial.print(",LEDValue=");
			Serial.print(LEDValue);
			Serial.print(",value=");
			Serial.print(value);
			Serial.print(",rlen=");
			Serial.println(rlen);
#endif
		}
	}
	
	
	analogWrite(Do_LED2_R, color[0][COLOR_R]);
	analogWrite(Do_LED2_G, color[0][COLOR_G]);
	analogWrite(Do_LED2_B, color[0][COLOR_B]);
	
	analogWrite(Do_LED3_R, color[1][COLOR_R]);
	analogWrite(Do_LED3_G, color[1][COLOR_G]);
	analogWrite(Do_LED3_B, color[1][COLOR_B]);
	/*
	static unsigned int tickPWM=0;
	if (millis()-tickPWM>3) {
		static byte counter=0;
		counter=(counter+1)%4;
		SetLED(Do_LED2_R, counter, correctPWM[color[0][COLOR_R]]);
		SetLED(Do_LED2_G, counter, correctPWM[color[0][COLOR_G]]);
		SetLED(Do_LED2_B, counter, correctPWM[color[0][COLOR_B]]);
		
		SetLED(Do_LED3_R, counter, correctPWM[color[1][COLOR_R]]);
		SetLED(Do_LED3_G, counter, correctPWM[color[1][COLOR_G]]);
		SetLED(Do_LED3_B, counter, correctPWM[color[1][COLOR_B]]);
	}
	*/
}


inline void SetLED(int Do_LED, unsigned int counter, unsigned int value){
	if (counter==0) {
		if (value<256) {
			analogWrite(Do_LED, value);
		} else {
			analogWrite(Do_LED, 255);
		}
	} else if (counter==1) {
		if (value<256) {
			analogWrite(Do_LED, 0);
		} else if (value<512) {
			analogWrite(Do_LED, value-256);
		} else {
			analogWrite(Do_LED, 255);
		}
	} else if (counter==2) {
		if (value<512) {
			analogWrite(Do_LED, 0);
		} else if (value<768) {
			analogWrite(Do_LED, value-512);
		} else {
			analogWrite(Do_LED, 255);
		}
	} else if (counter==3) {
		if (value<768) {
			analogWrite(Do_LED, 0);
		} else {
			analogWrite(Do_LED, value-768);
		}
	}
}
